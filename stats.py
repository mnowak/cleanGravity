# -*- coding: utf-8 -*-
"""
Function used for chi2 statistics (covariance extraction in particular)
@author: mnowak
"""

import numpy as np


def chi2Estimate(chi2Map, x = None, y = None, level = 2.28, fit_window_length = 16, center = None):
    """
    Extract the covariance matrix from the chi2Map

    Args:
      chi2Map: the values of the chi2Map
      x (float, optional): the x coordinates. Pixels are used if None
      y (float, optional): the x coordinates. Pixels are used if None
      level (float, optional): level value used. 2.28 for a true 2d map
      fit_window_length (int, optional): length in pixel of the subwindow used for fitting the map
      center (float[2], optional): bypass the minimum extraction and used these center coorinates instead
    """
    n, m = np.shape(chi2Map)
    if x is None:
        x = np.array(range(n))
    if y is None:        
        y = np.array(range(m))
    if center is None:
        ind = np.where(chi2Map == np.min(chi2Map))
        i, j = int(ind[0]), int(ind[1])
        center = (x[i], y[j])
    X0, Y0 = center
    d = int(fit_window_length/2.0)
    xx, yy = np.meshgrid(x[i-d:i+d], y[j-d:j+d])
    cc = chi2Map[i-d:i+d, j-d:j+d]
    Y = cc.ravel()-np.min(cc)
    X = np.zeros([len(Y), 3])
    X[:, 0] = (xx.ravel()-X0)**2
    X[:, 1] = (yy.ravel()-Y0)**2
    X[:, 2] = 2*(xx.ravel()-X0)*(yy.ravel()-Y0)
    
    A = np.dot(np.linalg.pinv(X), Y)
    a, c, b = A
    M = np.array([[a, b], [b, c]])
    val, vec = np.linalg.eig(M)
    
    covar = np.dot(np.dot(vec, np.diag(level/val)), vec.T)
    
    return center, covar

