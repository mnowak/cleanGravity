# -*- coding: utf-8 -*-
"""
A class to load the data from a gravity .astrored file.
@author: mnowak
"""
import numpy as np
import astropy.io.fits as fits
from .gravityGlobals import *

class GenericOiFits(object):
    """ 
    A generic class to load generic quantities (date, header, wavelength range, etc) from an OI in a fits file.

    Args:
      filename (str): the name of the file from which to load the data
      oiname (str): a name for this oi
      nchannel (int): number of channels (should be 4 channels if OI_FLUX, 6 channels if OI_VIS, for example)
      extension (int): extension for the OI, used in calls to astropy.io.fits.getdata. Usually 10 or 11 depending on polarization
      reduction (str): the level of reduction on the data. Can be 'astrored' or 'scivis'
      mode (str): the interferometric mode. "singlefield" or "dualfield"

    Attributes:
      filename, oiname, nchannel, extension, reduction, mode: from initialization
      ndit (int): number of DITs retrived from the header
      fits (atropy.fits): the fits OI loaded in astropy format
      nwav (int): number of wavelength pixels
      wav (float[nwav]): wavelength values (increasing order, in m)
      
    """
    def __init__(self, filename = None, oiname = None, nchannel = None, extension = None, reduction = None, mode = None):
        if (reduction is None):
            raise Exception("reduction argument cannot be left as None! Use 'astrored' or 'scivis'")
        if (mode is None):
            raise Exception("mode argument cannot be left as None! Use 'singlefield' or 'dualfield'")        
        # store parameters used at init
        self.mode = mode
        self.reduction = reduction
        self.filename = filename.split('/')[-1].split('.')[0] 
        self.nchannel = nchannel
        self.extension = extension
        self.oiname = oiname
        # load header of fits file
        header = fits.getheader(filename)
        if reduction == "astrored":
            try:
                self.ndit = header["HIERARCH ESO TPL NDIT OBJECT"]
            except:
                self.ndit = header["HIERARCH ESO TPL NDIT SKY"]                
        if reduction == "scivis":
            self.ndit = 1 # when scivis, consider that there is only one dit
        # load fits file
        self.fits = fits.getdata(filename, oiname, extension)
        # load wavelengths and some other generic stuff
        wav = fits.getdata(filename, WAVELENGTH_OI_NAME, extension).field('EFF_WAVE')
        self.wav = wav
        self.nwav = len(self.wav)        
        return None

    def loadData(self, fieldname):
        """Load the field from the fits file and return an array with the data of shape (ndit * nchannel * nwav), with ndit = 1 if reduction is scivis
        
        Args:
          fieldname (str): the name of the field to retrieve from the OI in the FITS file
        """
        data = self.fits.field(fieldname).reshape(self.ndit, self.nchannel, self.nwav)
        return data
