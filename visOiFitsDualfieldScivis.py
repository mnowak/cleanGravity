# -*- coding: utf-8 -*-
"""
A class implementing VisOi with mode = "dualfield" and reduction 'scivis', with some additional fitting methods
@author: mnowak
"""
import numpy as np
import astropy.io.fits as fits
from .genericOiFits import GenericOiFits 
from .visOiFits import VisOiFits
try:
    import patiencebar
    PBAR = True
except:
    PBAR = False


class VisOiFitsDualfieldScivis(VisOiFits):
    """
    A class implementing VisOi with mode = "dualfield" and reduction 'scivis', with some additional fitting methods
    """
    def __init__(self, filename, extension = None):
        super(VisOiFitsDualfieldScivis, self).__init__(filename = filename, extension = extension, mode = 'dualfield', reduction = "scivis")
        return None

    def fitVisAmpByChannel(self, xvalue, yvalue, order = 2, radec = False, ampInst = None, spectrum = None, error = True):
        wav = self.wav
        if ampInst is None:
            ampInst = np.ones([self.nchannel, self.nwav])
        if spectrum is None:
            spectrum = np.ones(self.nwav)
        if (radec == False):
            theta_rad = xvalue/360.0*2*np.pi
            this_u = (np.sin(theta_rad)*self.uCoordMean + np.cos(theta_rad)*self.vCoordMean)/1e7            
            wavelet = spectrum[None, :]*ampInst*np.exp(-1j*2*np.pi*this_u*1e7*yvalue/3600.0/360.0/1000.0*2*np.pi)
        else:
            this_u = (xvalue*self.uCoordMean + yvalue*self.vCoordMean)/1e7
            wavelet = spectrum[None, :]*ampInst*np.exp(-1j*2*np.pi*this_u*1e7/3600.0/360.0/1000.0*2*np.pi)
        Yfit = np.zeros([self.nchannel, self.nwav])
        # ignore all nan points in the fit by setting all them to 0
        visAmp = np.copy(self.visAmp)[0, :, :]
        freq = np.copy(self.freq)[0, :, :]/1e7
        visAmpErr = np.copy(self.visAmpErr)[0, :, :]
        ind = np.where(np.isnan(visAmp))
        visAmp[ind] = 0
        freq[ind] = 0
        wavelet[ind] = 0
        cosine = np.real(wavelet)
        sine = np.imag(wavelet)        
        X = np.zeros([order+3, self.nwav])
        Atotal = np.zeros([self.nchannel, order+3])
        for c in range(self.nchannel):
            if (error == True):
                for k in range(order+1):
                    X[k, :] = freq[c, :]**k/visAmpErr[c, :]
                X[order+1, :] = cosine[c, :]/visAmpErr[c, :]
                X[order+2, :] = sine[c, :]/visAmpErr[c, :]                
            else:
                for k in range(order+1):
                    X[k, :] = freq[c, :]**k
                X[order+1, :] = cosine[c, :]
                X[order+2, :] = sine[c, :]                
            if error == True:
                Y = visAmp[c, :]/visAmpErr[c, :]
            else:
                Y = visAmp[c, :]
            A = np.dot(Y, np.linalg.pinv(X))
            Atotal[c, :] = A
            result = np.dot(A, X)
            if (error == True):
                Yfit[c, :] = result * visAmpErr[c, :]
            else:
                Yfit[c, :] = result
        if (error == True):
            sqrsum = np.nansum((visAmp - Yfit)**2/(visAmpErr)**2)
        else:
            sqrsum = np.nansum((visAmp - Yfit)**2)
        return (Yfit, cosine, Atotal, sqrsum)


    def computeVisAmpChi2Map(self, xvalues, yvalues, radec = False, ampInst = None, order = 2):
        nxvalues = len(xvalues)
        nyvalues = len(yvalues)
        chi2Map = np.zeros([nxvalues, nyvalues])
        xBest = xvalues[0]
        yBest = yvalues[0]
        chi2Min = np.inf
        bestFit = None
        bestCosine = None
        Abest = None
        if PBAR:
            pbar = patiencebar.Patiencebar(valmax = len(xvalues), title = "Calculating the chi-square map...")
        for k in range(nxvalues):
            if PBAR:
                pbar.update()
            for i in range(nyvalues):
                Yfit, cosine, Atotal, sqrsum = self.fitVisAmpByChannel(xvalues[k], yvalues[i], ampInst = ampInst, error = True, radec = radec, order = order)
                chi2Map[k, i] = sqrsum
                if sqrsum < chi2Min:
                    chi2Min = sqrsum
                    xBest = xvalues[k]
                    yBest = yvalues[i]
                    bestFit = Yfit
                    bestCosine = cosine
                    Abest = Atotal
        self.fit = {"chi2Map": chi2Map, "chi2Min": chi2Min, "xvalus":xvalues, "yvalues": yvalues, "bestFit": bestFit, "xBest": xBest, "yBest": yBest, "radec": radec, "Abest": Abest}
        self.visAmpFit = bestFit
        self.cosine = bestCosine        
        if (radec == False):
            print("Best fit: "+str(xBest)+" deg, "+str(yBest)+" mas")
        else:
            print("Best fit: RA="+str(xBest)+" mas, DEC="+str(yBest)+" mas")            
        return None
        

