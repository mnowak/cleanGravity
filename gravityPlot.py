# -*- coding: utf-8 -*-
"""
A small package providing just a few plot function to help display gravity data
@author: mnowak
"""
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import gridspec

def reImPlot(xdata, ydata, subtitles = None, fig = None, ylim = None, xlabel = None, **kwargs):
    s = np.shape(xdata)
    if fig is None:
        fig = plt.figure()
    if not(subtitles is None):
        grid = gridspec.GridSpec(s[0], 2)
        grid.update(left=0.1, right=0.95, top= 0.96, bottom=0.08, wspace=0.0,hspace=0.0)
        ax_reals = [fig.add_subplot(grid[k, 0]) for k in range(s[0])]
        ax_imags = [fig.add_subplot(grid[k, 1]) for k in range(s[0])]        
    else:
        ax_reals = fig.axes[0:s[0]]
        ax_imags = fig.axes[s[0]:]        
    nchannel = np.shape(xdata)[0]
    for k in range(nchannel):
        ax_reals[k].plot(xdata[k, :], np.real(ydata[k, :]), **kwargs)
        ax_imags[k].plot(xdata[k, :], np.imag(ydata[k, :]), **kwargs)
        if not(subtitles is None):
            ax_reals[k].set_ylabel(subtitles[k])
            ax_imags[k].yaxis.tick_right()
            ax_imags[k].yaxis.set_label_position("right")                            
        if ( (k==0) and not(fig is None) ):
            ax_reals[k].set_title("Real part of coherent flux")
            ax_imags[k].set_title("Imaginary part of coherent flux")
        if not(ylim is None):
            ax_reals[k].set_ylim(ylim)
            ax_imags[k].set_ylim(ylim)            
    if not(xlabel is None):
        ax_reals[-1].set_xlabel(xlabel)
        ax_imags[-1].set_xlabel(xlabel)
    return ax_reals, ax_imags

def modPhasePlot(xdata, ydata, subtitles = None, fig = None, xlabel = None, **kwargs):
    s = np.shape(xdata)
    if fig is None:
        fig = plt.figure()
    if not(subtitles is None):
        grid = gridspec.GridSpec(s[0], 2)
        grid.update(left=0.1, right=0.95, top= 0.96, bottom=0.08, wspace=0.0,hspace=0.0)
        ax_mods = [fig.add_subplot(grid[k, 0]) for k in range(s[0])]
        ax_phases = [fig.add_subplot(grid[k, 1]) for k in range(s[0])]        
    else:
        ax_mods = fig.axes[0:s[0]]
        ax_phases = fig.axes[s[0]:]        
    nchannel = np.shape(xdata)[0]
    for k in range(nchannel):
        ax_mods[k].plot(xdata[k, :], np.abs(ydata[k, :]), **kwargs)
        ax_phases[k].plot(xdata[k, :], np.angle(ydata[k, :])/np.pi*180, **kwargs)
        if not(subtitles is None):
            ax_mods[k].set_ylabel(subtitles[k])
        if ( (k==0) and not(fig is None) ):
            ax_mods[k].set_title("Amplitude of the coherent flux")
            ax_phases[k].set_title("Phase of the coherent flux")            
    if not(xlabel is None):
        ax_mods[-1].set_xlabel(xlabel)        
        ax_phases[-1].set_xlabel(xlabel)
    return ax_mods, ax_phases

def reImWaterfall(data, fig = None, subtitles = None, vmin = None, vmax = None, xlabel = "Wavelength"):
    s = np.shape(data)
    if fig is None:
        fig = plt.figure()
    if not(subtitles is None):
        grid = gridspec.GridSpec(s[1], 2)
        grid.update(left=0.1, right=0.95, top= 0.96, bottom=0.08, wspace=0.0,hspace=0.0)
        ax_reals = [fig.add_subplot(grid[k, 0]) for k in range(s[1])]
        ax_imags = [fig.add_subplot(grid[k, 1]) for k in range(s[1])]        
    else:
        ax_reals = fig.axes[0:s[1]]
        ax_imags = fig.axes[s[1]:]        
    nchannel = np.shape(data)[1]
    for k in range(nchannel):
        ax_reals[k].imshow(np.real(data[:, k, :]), vmin = vmin, vmax = vmax, interpolation="antialiased", aspect = "auto")
        ax_imags[k].imshow(np.imag(data[:, k, :]), vmin = vmin, vmax = vmax, interpolation="antialiased", aspect = "auto")
        if not(subtitles is None):
            ax_reals[k].set_ylabel(subtitles[k])
        if ( (k==0) and not(fig is None) ):
            ax_reals[k].set_title("Real part of coherent flux")
            ax_imags[k].set_title("Imaginary part of coherent flux")
    if not(subtitles is None):
        ax_reals[-1].set_xlabel(xlabel)        
        ax_imags[-1].set_xlabel(xlabel)

def whirlpoolPlot(wavdata, ydata, subtitles = None, fig = None):
    s = np.shape(ydata)
    if fig is None:
        fig = plt.figure()
    if not(subtitles is None):
        grid = gridspec.GridSpec(s[0]//2, 2)
        grid.update(left=0.1, right=0.95, top= 0.96, bottom=0.08, wspace=0.0,hspace=0.0)
        axs = [fig.add_subplot(grid[k, 0], projection = "3d") for k in range(s[0]//2)]
        axs = axs + [fig.add_subplot(grid[k, 1], projection = "3d") for k in range(s[0]//2)]        
    else:
        axs = fig.axes
    nchannel = np.shape(ydata)[0]
    for k in range(nchannel):
        axs[k].plot3D(wavdata, np.real(ydata[k, :]), np.imag(ydata[k, :]), '.-')
        axs[k].set_xlabel("Wavelength ($\\mu\\mathrm{m}$)")        
        if not(subtitles is None):
            axs[k].set_ylabel(subtitles[k])
    if not(subtitles is None):
        fig.suptitle('Coherent flux')        
    return None        


def waterfall(xdata, ydata, subtitles = None, fig = None, xlabel = None):
    s = np.shape(ydata)
    if fig is None:
        fig = plt.figure()
    if not(subtitles is None):
        grid = gridspec.GridSpec(s[1]//2, 2)
        grid.update(left=0.1, right=0.9, top= 0.96, bottom=0.08, wspace=0.1, hspace=0.1)
        axs = [fig.add_subplot(grid[k, 0]) for k in range(s[1]//2)]
        axs = axs + [fig.add_subplot(grid[k, 1]) for k in range(s[1]//2)]        
    else:
        axs = fig.axes
    nchannel = np.shape(ydata)[1]
    for k in range(nchannel):
        axs[k].imshow(ydata[:, k, :], extent = [np.min(xdata[k, :]), np.max(xdata[k, :]), 0, np.shape(ydata)[0]], aspect='auto')
        if not(subtitles is None):
            axs[k].set_ylabel(subtitles[k])
    if not(xlabel is None):
        axs[-1].set_xlabel(xlabel)
        axs[s[1]/2-1].set_xlabel(xlabel)        
    return None        


def baselinePlot(xdata, ydata, yerr = None, subtitles = None, fig = None, xlabel = None, ylabel = None, ylim = None, fmt = '-'):
    s = np.shape(ydata)
    if fig is None:
        fig = plt.figure()
    if not(subtitles is None):
        grid = gridspec.GridSpec(s[0]//2, 2)
        grid.update(left=0.1, right=0.95, top= 0.96, bottom=0.08, wspace=0.0,hspace=0.0)
        axs = [fig.add_subplot(grid[k, 0]) for k in range(s[0]//2)]
        axs = axs + [fig.add_subplot(grid[k, 1]) for k in range(s[0]//2)]        
    else:
        axs = fig.axes
    nchannel = np.shape(ydata)[0]
    for k in range(nchannel):
        if yerr is None:
            axs[k].plot(xdata[k, :], ydata[k, :], fmt)
        else:
            axs[k].errorbar(xdata[k, :], ydata[k, :], yerr = yerr[k, :], fmt = '.')            
        if not(subtitles is None):
            axs[k].set_ylabel(subtitles[k])
        if ( (k in [0, nchannel//2]) and not(ylabel is None) ):
            axs[k].set_title(ylabel)
        if not(ylim is None):
            axs[k].set_ylim(ylim)
    if not(subtitles is None):
        axs[-1].set_xlabel(xlabel)
        axs[s[0]//2-1].set_xlabel(xlabel)
    for k in range(nchannel//2, nchannel):
        axs[k].yaxis.tick_right()
        axs[k].yaxis.set_label_position("right")
    return axs        

def uvMap(uCoord, vCoord, targetCoord = None, fig = None, ax = None, legend = None, symmetric = False, colors = None, lim=None):
    """
    Plot the U-V coordinates, possibly ontop of a fringe background. Target coord in mas (ra, dec)
    """
    if (fig is None) and (ax is None):
        fig = plt.figure(figsize = (5, 5))
        ax = plt.subplot(111)
    if colors is None:
        colors = ['C'+str(k) for k in range(0, 6)]
    r = 2*np.nanmax(np.sqrt(uCoord**2+vCoord**2))*1e-7
    if not(lim is None):
        r = lim*1e-7        
    ulim = np.nanmax(np.abs(uCoord))
    vlim = np.nanmax(np.abs(vCoord))
    if lim is None:
        lim = np.nanmax([ulim, vlim])
    u = np.linspace(-1.1*lim, 1.1*lim, 1000)
    v = np.linspace(-1.1*lim, 1.1*lim, 1000)
    uu, vv = np.meshgrid(u, v)
    for c in range(uCoord.shape[0]):
        ax.scatter(uCoord[c, :]*1e-7, vCoord[c, :]*1e-7, marker=".", facecolor = colors[c], )
    if symmetric:
        for c in range(uCoord.shape[0]):        
            ax.scatter(-uCoord[c, :]*1e-7, -vCoord[c, :]*1e-7, marker=".", facecolor = colors[c])
    if not(legend is None):
        ax.legend(legend)
    if not(targetCoord is None):
        (ra, dec) = targetCoord
        fringesMap = np.cos(2*np.pi*((ra/360.0/3600.0/1000.0*2*np.pi)*uu+(dec/360.0/3600.0/1000.0*2*np.pi)*vv))
        ax.imshow(fringesMap, extent = [np.min(u)*1e-7, np.max(u)*1e-7, np.min(v)*1e-7, np.max(v)*1e-7], vmin=-2, vmax=2, aspect='equal', cmap="gray", origin = 'lower')
        vdir = np.array([dec/360.0/3600.0/1000.0*2*np.pi, -ra/360.0/3600.0/1000.0*2*np.pi])
        if vdir[0] < 0:
            vdir = -vdir
        vdirnorm = vdir/(vdir[0]**2+vdir[1]**2)**0.5
        x = [-r, r]
#        while x[0]<r:
#            plt.plot([x[0]-r*vdirnorm[0], x[0]+r*vdirnorm[0]], [x[1]-r*vdirnorm[1], x[1]+r*vdirnorm[1]], 'C1', zorder = 1, alpha = 0.)
#            x = x+np.array([vdirnorm[1], -vdirnorm[0]])#/2/np.pi
    # axes
    ax.spines['left'].set_position('center')
    ax.spines['bottom'].set_position('center')
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')    
    plt.xlim([-1.1*r/2, 1.1*r/2])
    plt.ylim([-1.1*r/2, 1.1*r/2])
    plt.xlabel("U coordinate ($\\times{}10^7 \\mathrm{ rad}^{-1}$)")
    plt.ylabel("V coordinate ($\\times{}10^7 \\mathrm{ rad}^{-1}$)")
    ax.xaxis.set_label_coords(0.75, 0.45)
    ax.yaxis.set_label_coords(0.45, 0.75)
    # Clip
    theta = np.linspace(0,2*np.pi,100);      # A vector of 100 angles from 0 to 2*pi
    xCircle = r/2*1.2*np.cos(theta);              # x coordinates for circle
    yCircle = r/2*1.2*np.sin(theta);              # y coordinates for circle
    xSquare = [r, r, -r, -r, r, r];         # x coordinates for square
    ySquare = [0, -r, -r, r, r, 0];         # y coordinates for square
    hp = plt.fill(list(xCircle)+xSquare, list(yCircle)+ySquare, 'w');
    return None
                                                                        
