# -*- coding: utf-8 -*-
"""
A class to load the data from a gravity .astrored file.
@author: mnowak
"""
import numpy as np
import astropy.io.fits as fits
from datetime import datetime
from .gravityGlobals import *

class GenericOiFits(object):
    """
    A generic class to load any type of gravity OI.
    """
    def __init__(self, filename = None, oiname = None, nchannel = None, extension = None, reduction = None):
        """
        A generic function to load generic quantities (date, header, wavelength range, etc) from an OI in a fits file.
        @param filename: the name of the file from which to load the data
        @param oiname: a name for this oi
        @param nchannel: number of channels (4 channels if OI_FLUX, 6 channels if OI_VIS, for example)
        @param extension: extension for the OI, used in calls to astropy.io.fits.getdata
        @param reduction: can be 'astrored' or 'scivis'. If 'scivis' is used, 
        """
        if (reduction is None):
            raise Exception("reduction argument cannot be left as None! Use 'astrored' or 'scivis'")
        # store parameters used at init
        self.reduction = reduction
        self.filename = filename.split('/')[-1].split('.')[0] 
        self.nchannel = nchannel
        self.extension = extension
        self.oiname = oiname
        # load header of fits file
        header = fits.getheader(filename)
        if reduction == "astrored":
            try:
                self.ndit = header["HIERARCH ESO TPL NDIT OBJECT"]
            except:
                self.ndit = header["HIERARCH ESO TPL NDIT SKY"]                
        if reduction == "scivis":
            self.ndit = 1 # when scivis, consider that there is only one dit
        # load fits file
        self.fits = fits.getdata(filename, oiname, extension)
        # load wavelengths and some other generic stuff
        wav = fits.getdata(filename, WAVELENGTH_OI_NAME, extension).field('EFF_WAVE')
        self.wav = wav
        self.nwav = len(self.wav)
#        self.dit = self.header["HIERARCH ESO DET2 SEQ1 DIT"] # dit time in seconds
#        self.datetime = datetime.strptime(self.header["DATE-OBS"], TIME_ISO_FORMAT)
#        self.time = (self.datetime - datetime(1970, 1, 1, 0, 0, 0)).total_seconds()
        return None

    def loadData(self, fieldname):
        """
        Load the given field from the fits file and return the corresponding array of data in the right shape
        (ndit * nchannel * nwav), with ndit = 1 if reduction is scivis
        """
        data = self.fits.field(fieldname).reshape(self.ndit, self.nchannel, self.nwav)
        return data

    # DEPRECATED
    def _getSerie(self, serie, dit = -1):
        """
        @param serie: the name of the serie to retreive. Should be overwritten in all daughter class
        """
        return None

    def deleteChannel(self, channel):
        """
        Delete a given channel from the data
        @param list channels: a list of int giving the indices of channels to delete (warning: after deletion of certain channels, indices are shifter to the left)
        """
        for key in self.data.keys():
            self.data[key] = self.data[key][:, [i for i in self.nchannel if not(i in channels)], :]
        self.nchannel = self.nchannel - 1
        return None

    def deleteDit(self, dit):
        """
        Delete a given dit from the data
        @param list channels: a list of int giving the indices of channels to delete (warning: after deletion of certain channels, indices are shifter to the left)
        """
        for key in self.data.keys():
            self.data[key] = self.data[key][[i for i in self.nchannel if not(i in channels)], :, :]
        self.ndit = self.ndit - 1
        return None
    
    def percentileFilter(self, serie, percentile_low, percentile_high, channels = None, dits = None):
        """
        Filter the data using a percentile to reject bad points
        @param serie: the serie on which to use the filter
        @param percentile_low: lower percentile (all point below will be rejected)
        @param percentile_high: lower percentile (all point above will be rejected)
        @return data: filtered serie
        """
        if channels is None:
            channels = range(self.nchannel)
        if dits is None:
            dits = range(self.ndit)
        data = serie
        for dit in dits:
            for channel in channels:
                value_low = np.nanpercentile(np.abs(data[dit, channel, :]), percentile_low)               
                value_high = np.nanpercentile(np.abs(data[dit, channel, :]), percentile_high)
                for k in range(self.nwav):
                    if ((np.abs(data[dit, channel, k]) < value_low) or (np.abs(data[dit, channel, k]) > value_high)):
                        data[dit, channel, k] = float('nan')
        return data

    def runningMeanFilter(self, serie, length, channels = None, dits = None):
        """
        Filter the data using a running mean filter of given length 
        """
        if channels is None:
            channels = range(self.nchannel)
        if dits is None:
            dits = range(self.ndit)
        data = self._getSerie(serie)
        for dit in dits:
            for channel in channels:
                filtered = np.convolve(data[dit, channel, :], np.ones(length), mode = 'same')/np.convolve(np.ones(len(data[dit, channel, :])), np.ones(length), mode = 'same')
                data[dit, channel, :] = filtered
        return None

    def runningVarianceFilter(self, serie, length, threshold = 9.0, channels = None, dits = None):
        """
        Filter the data using a compatison the a running variance 
        """
        if channels is None:
            channels = range(self.nchannel)
        if dits is None:
            dits = range(self.ndit)
        data = self._getSerie(serie)
        for dit in dits:
            for channel in channels:
                runningMean = np.convolve(data[dit, channel, :], np.ones(length), mode = 'same')/np.convolve(np.ones(len(data[dit, channel, :])), np.ones(length), mode = 'same')
                dummy = np.abs(data[dit, channel, :] - runningMean)**2
                runningVariance = np.convolve(dummy, np.ones(length), mode = 'same')/np.convolve(np.ones(len(data[dit, channel, :])), np.ones(length), mode = 'same')
                ind = np.where(dummy > threshold * runningVariance)
                data[dit, channel, ind] = float('nan')
        return None


    def stdFilter(self, serie, threshold = 3.0, repeat = 1, channels = None, dits = None):
        """
        Filter the data using a compatison the a running variance 
        """
        if channels is None:
            channels = range(self.nchannel)
        if dits is None:
            dits = range(self.ndit)
        data = self._getSerie(serie)
        for dit in dits:
            for channel in channels:
                for k in range(repeat):
                    std = np.nanstd(np.abs(data[dit, channel, :]))
                    mean = np.nanmean(np.abs(data[dit, channel, :]))
                    ind = np.where(np.abs(np.abs(data[dit, channel, :]) - mean) > threshold * std)
                    data[dit, channel, ind] = float('nan')
        return None    
        

    def polyfit(self, serie, error = None, order = 2):
        """
        Fit a polynomial of given order to the given serie of data
        """
        Y = serie
        X = 0*self.wav[:, None] + 1
        for k in range(1, order+1):
            X = np.concatenate([X, self.wav[:, None]**k], axis = 1)
        if not(error is None):        
            E = self._getSerie(error)
            Y = Y/E
        Ymodel = np.zeros([self.ndit, self.nchannel, self.nwav])
        A = np.zeros([self.ndit, self.nchannel, order+1])
        sqrsums = np.zeros([self.ndit, self.nchannel])
        for i in range(self.ndit):
            for j in range(self.nchannel):
                if not(error is None):
                    X = X/E[i, j, :][:, None]
                Xinv = np.linalg.pinv(X)
                A[i, j, :] = np.dot(Xinv, Y[i, j, :])
                Ymodel[i, j, :] = np.dot(X, A[i, j, :])
                sqrsums[i, j] = np.sum((Y[i, j, :] - Ymodel[i, j, :])**2)
        if not(error is None):
            Ymodel = Ymodel * E        
        return [A, Ymodel, sqrsums]

