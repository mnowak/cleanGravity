# -*- coding: utf-8 -*-
"""
some global variables used thoughout the code
@author: mnowak
"""
import numpy as np

WAVELENGTH_OI_NAME = "OI_WAVELENGTH"
FLUX_OI_NAME = "OI_FLUX"
VIS_OI_NAME = "OI_VIS"
VIS2_OI_NAME = "OI_VIS2"
ARRAY_OI_NAME = "OI_ARRAY"
T3_OI_NAME = "OI_T3"

CLOSURENAMES = ["UT4-UT3-UT2", "UT4-UT2-UT1", "UT4-UT3-UT1", "UT3-UT2-UT1"]

TIME_ISO_FORMAT = "%Y-%m-%dT%H:%M:%S"

PHASE_REF_POLY_ORDER = 4 # the order of the polynomial fit used to fit the FT phase in the visOi data

# matrix D for transforming phase on each telescope to phase difference on each baseline
D = np.array([[ 1, -1,  0,  0],
              [ 1,  0, -1,  0],
              [ 1,  0,  0, -1],
              [ 0,  1, -1,  0],
              [ 0,  1,  0, -1],
              [ 0,  0,  1, -1]])

# matrix C for closure phases (null space of D)
C = np.array([[ 1, -1,  0,  1,  0,  0],
              [ 1,  0, -1,  0,  1,  0],                            
              [ 0,  1, -1,  0,  0,  1],
              [ 0,  0,  0,  1, -1,  1]])




