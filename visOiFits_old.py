#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A class to load the data from a gravity .astrored file visOiFits
@author: mnowak
"""
import numpy as np
import astropy.io.fits as fits
import patiencebar
from genericOiFits import GenericOiFits 

VIS_OI_NAME = "OI_VIS"
BASENAMES = ["UT4-UT3", "UT4-UT2", "UT4-UT1", "UT3-UT2", "UT3-UT1", "UT2-UT1"]

class VisOiFits(GenericOiFits):
    def __init__(self, filename, reduction = None):
        super(VisOiFits, self).__init__(filename = filename, oiname = VIS_OI_NAME, nchannel = 6, channel_names = BASENAMES, reduction = reduction)
        self.oiname = VIS_OI_NAME
        self.u=self.fits.field("UCOORD").reshape(self.ndit, self.nchannel)
        self.v=self.fits.field("VCOORD").reshape(self.ndit, self.nchannel)
        self.uCoord=self.u[:, :, None]/self.wav
        self.vCoord=self.v[:, :, None]/self.wav
        self.freq = np.abs(self.uCoord + 1j*self.vCoord)
        self.visData= self.loadData("VISDATA")
        self.visErr= self.loadData("VISERR")
        if reduction == "astrored":
            self.phaseRef = self.loadData("PHASE_REF")
            self.opdDisp = self.loadData("OPD_DISP")
            self.gDelayDisp = self.fits.field("GDELAY_DISP").reshape(self.ndit, self.nchannel)
            self.phaseDisp = self.loadData("PHASE_DISP")
            self.visRef = self.visData * np.exp(1j*self.phaseRef) * np.exp(-1j*2*np.pi*self.opdDisp/self.wav) # phaseRef correct for FT zero point; opdDisp correct for dispersion
        if reduction == "dualscivis":
            self.visAmp = self.loadData("VISAMP")
            self.visPhi = self.loadData("VISPHI")
            self.visAmpErr = self.loadData("VISAMPERR")
            self.visPhiErr = self.loadData("VISPHIERR")                        
            self.visRef = self.visData
        self.visRefFit = None
        self.visRefFitSpec = None
        return None

    def _getSerie(self, serie, dit = -1):
        res = None
        if serie == 'visRef':
            res = self.visRef
        if not(res is None):
            if dit == -1:
                res = res[:, :, :]
            else:
                res = res[dit, :, :]                            
        return res

    def computeMean(self):
        """
        Calculate ans store the mean of each attribute dataset
        """
        self.uCoordMean = np.nanmean(self.uCoord, axis = 0)
        self.vCoordMean = np.nanmean(self.vCoord, axis = 0)        
        self.freqMean = np.abs(self.uCoordMean + 1j*self.vCoordMean)
        self.visDataMean= np.nanmean(self.visData, axis = 0)
        self.visErrMean= np.sqrt(np.nansum(self.visErr**2, axis = 0))/self.ndit
        if self.reduction == "astrored":
            self.phaseRefMean = np.nanmean(self.phaseRef, axis = 0)
            self.opdDispMean = np.nanmean(self.opdDisp, axis = 0)
            self.gDelayDispMean = np.nanmean(self.gDelayDisp, axis = 0)
            self.phaseDispMean = np.nanmean(self.phaseDisp, axis = 0)
        if (self.reduction == "dualscivis"):
            self.visAmpMean = np.mean(self.visAmp, axis = 0)
            self.visPhiMean = np.mean(self.visPhi, axis = 0)
            self.visAmpMeanErr = np.mean(self.visAmpErr, axis = 0)
            self.visPhiMeanErr = np.mean(self.visPhiErr, axis = 0)                                        
        self.visRefMean = np.nanmean(self.visRef, axis = 0)        
        if not(self.visRefFit is None):
            self.visRefFitMean = np.nanmean(self.visRefFit, axis = 0)
        if not(self.visRefFitSpec is None):
            self.visRefFitSpecMean = np.nanmean(self.visRefFitSpec, axis = 0)
        return None

    def recenterPhase(self, xvalue, yvalue, radec = False):
        """
        Recenter visRef and visRefError on to the given position by shifting the phases
        """
        if (radec == False):
            theta_rad = xvalue/360.0*2*np.pi
            this_u = (np.sin(theta_rad)*self.uCoord + np.cos(theta_rad)*self.vCoord)/1e7            
            phase = 2*np.pi*this_u*1e7*separation/3600.0/360.0/1000.0*2*np.pi
        else:
            this_u = (xvalue*self.uCoord + yvalue*self.vCoord)/1e7
            phase = 2*np.pi*this_u*1e7/3600.0/360.0/1000.0*2*np.pi
        self.visRef = self.visRef * np.exp(1j*phase)
        if not(self.visRefFit is None):
            self.visRefFit = self.visRefFit * np.exp(1j*phase)
        if not(self.visRefFitSpec is None):
            self.visRefFitSpec = self.visRefFitSpec * np.exp(1j*phase)
        self.visErr = self.visErr * np.exp(1j*phase)
        return None

    def fitStarDitByDit(self, visStar, order = 2, ra = 0, dec = 0, saturation = None):
        """
        Fit a star model to the visRef. The star model is the given visStar multiplied by a polynomial in
        wavelength of the given order.
        """
        # create the model for the star
        ampStar = np.abs(visStar)
        phaseStar = np.angle(visStar)
        X = np.zeros([self.nchannel, 2*(order+1), self.nwav], 'complex')
        this_u = np.mean((ra*self.uCoord + dec*self.vCoord)/1e7, axis = 0)
        phaseFactor = np.exp(-1j*2*np.pi*this_u*1e7/3600.0/360.0/1000.0*2*np.pi)
        for c in range(self.nchannel):
            for k in range(order+1):
                X[c, k, :] = ampStar[c, :]*(1e7*(self.wav - np.mean(self.wav)))**k
#                X[c, k, :] = ampStar[c, :]*(1.9e-6/self.wav)**k
            for k in range(order+1):
#                X[c, order+1+k, :] = ampStar[c, :]*(1e7*(self.wav - np.mean(self.wav)))**k * phaseFactor[c, :]
#                X[c, order+1+k, :] = 1*(1.9e-6/self.wav)**k*0
                if (saturation == "squared"):
                    X[c, order+1+k, :] = (ampStar[c, :]**2)*(1.9e-6/self.wav)**k
                if (saturation == "constant"):
                    X[c, order+1+k, :] = 1*(1e7*(self.wav - np.mean(self.wav)))**k
                else:
                    X[c, order+1+k, :] = 0
        # store the model as is
        self.visStarModel = X
        # prepare the fit
        Yfit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        coeffs = np.zeros([self.ndit, self.nchannel, 2*(order+1)], 'complex')
        # star to fit, dit by dit, channel by channel
        for dit in range(self.ndit):    
            for c in range(self.nchannel):
                Y = self.visRef[dit, c, :]
                # ignore all nan points in the fit by setting all them to 0
                X = np.copy(self.visStarModel[c, :, :])
                ind = np.where(np.isnan(Y))
                Y[ind] = 0
                for k in range(order+1):
                    X[k, ind] = 0
                A = np.dot(Y, np.linalg.pinv(X))
                coeffs[dit, c, :] = A
                Yfit[dit, c, :] = np.dot(A, X)
                Yfit[dit, c, ind] = float('nan') + float('nan')*1j
        self.coeffs = coeffs
        # store everything for plotting later on
        self.visStar = Yfit
        self.visStarMean = np.nanmean(Yfit, axis = 0)
        Yres = self.visRef - Yfit
        self.visRef = Yres    
        self.visRefMean = np.nanmean(Yres, axis = 0)        
        return None
                             
    def fitPlanetDitByDit(self, xvalue, yvalue, visInst = None, error = True, spectrum = None, radec = False, useStarModel = False, fitMetrology = False):
        """
        Fit each dit using a model made of a given spectrum multiplied by a polynomial (of given order)
        and a wavelet corresponding to given position angle (deg) and separation (mas)
        visInst is the instrumental phase and amplitude
        You can also give a planet spectrum.
        """
        wav = self.wav
        if visInst is None:
            ampInst = np.ones(np.shape(self.visRef))
            phaseInst = np.zeros(np.shape(self.visRef))
        else:
            ampInst = np.abs(visInst)
            phaseInst = np.angle(visInst)
        if spectrum is None:
            spectrum = np.ones(self.nwav)
        if (radec == False):
            theta_rad = xvalue/360.0*2*np.pi
            this_u = (np.sin(theta_rad)*self.uCoord + np.cos(theta_rad)*self.vCoord)/1e7            
            wavelet = spectrum[None, None, :]*ampInst*np.exp(-1j*2*np.pi*this_u*1e7*separation/3600.0/360.0/1000.0*2*np.pi)
        else:
            this_u = (xvalue*self.uCoord + yvalue*self.vCoord)/1e7
            wavelet = spectrum[None, None, :]*ampInst*np.exp(-1j*2*np.pi*this_u*1e7/3600.0/360.0/1000.0*2*np.pi)
        if useStarModel == True:
            for c in range(self.nchannel):
                wavelet[:, c, :] = wavelet[:, c, :] - np.dot(np.dot(wavelet[:, c, :], np.linalg.pinv(self.visStarModel[c, :, :])), self.visStarModel[c, :, :])
        Yfit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        # ignore all nan points in the fit by setting all them to 0
        visRef = np.copy(self.visRef)
        ind = np.where(np.isnan(visRef))
        visRef[ind] = 0
        wavelet[ind] = 0
        for dit in range(self.ndit):
            Y = np.zeros([1, 2*self.nchannel*self.nwav])
            X = np.zeros([self.nchannel, 2*self.nchannel*self.nwav])                        
            for c in range(self.nchannel):
                if (error == True):
                    Y[0, 2*c*self.nwav:(2*c+1)*self.nwav] = np.real(visRef[dit, c, :])/np.real(self.visErr[dit, c, :])
                    Y[0, (2*c+1)*self.nwav:(2*c+2)*self.nwav] = np.imag(visRef[dit, c, :])/np.imag(self.visErr[dit, c, :])
                    X[c, 2*c*self.nwav:(2*c+1)*self.nwav] = np.real(wavelet[dit, c, :])/np.real(self.visErr[dit, c, :])
                    X[c, (2*c+1)*self.nwav:(2*c+2)*self.nwav] = np.imag(wavelet[dit, c, :])/np.imag(self.visErr[dit, c, :])
                else:
                    Y[0, 2*c*self.nwav:(2*c+1)*self.nwav] = np.real(visRef[dit, c, :])
                    Y[0, (2*c+1)*self.nwav:(2*c+2)*self.nwav] = np.imag(visRef[dit, c, :])
                    X[c, 2*c*self.nwav:(2*c+1)*self.nwav] = np.real(wavelet[dit, c, :])
                    X[c, (2*c+1)*self.nwav:(2*c+2)*self.nwav] = np.imag(wavelet[dit, c, :])
            A = np.dot(Y, np.linalg.pinv(X))
            result = np.dot(A, X)
            for c in range(self.nchannel):
                if (error == True):
                    Yfit[dit, c, :] = result[0, 2*c*self.nwav:(2*c+1)*self.nwav]*np.real(self.visErr[dit, c, :]) + 1j*result[0, (2*c+1)*self.nwav:(2*c+2)*self.nwav]*np.imag(self.visErr[dit, c, :])
                else:
                    Yfit[dit, c, :] = result[0, 2*c*self.nwav:(2*c+1)*self.nwav] + 1j*result[0, (2*c+1)*self.nwav:(2*c+2)*self.nwav]                    
        Yres = self.visRef - Yfit
        if (error == True):
            sqrsum = np.nansum(np.real((Yfit - self.visRef))**2/np.real(self.visErr)**2 + np.imag((Yfit - self.visRef))**2/np.imag(self.visErr)**2)
        else:
            sqrsum = np.nansum(np.abs((Yfit - self.visRef))**2)
        if (fitMetrology == True):
            Yres[ind] = 0.
            Yresfit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')            
            ntel = 4
            P = np.array([[ 1,  1,  1,  0,  0,  0,],
                          [-1,  0,  0,  1,  1,  0,],
                          [ 0, -1,  0, -1,  0,  1,],
                          [ 0,  0, -1,  0, -1, -1,]])
            X = np.zeros([ntel, 2*self.nchannel*self.nwav])
            Y = np.zeros([1, 2*self.nchannel*self.nwav])
            for dit in range(self.ndit):
                for c in range(self.nchannel):
                    if (error == True):
                        Y[0, 2*c*self.nwav:(2*c+1)*self.nwav] = np.real(Yres[dit, c, :])/np.real(self.visErr[dit, c, :])
                        Y[0, (2*c+1)*self.nwav:(2*c+2)*self.nwav] = np.imag(Yres[dit, c, :])/np.imag(self.visErr[dit, c, :])
                        for t in range(ntel):
                            X[t, 2*c*self.nwav:(2*c+1)*self.nwav] = P[t, c]*np.imag(Yfit[dit, c, :])/np.real(self.visErr[dit, c, :])*(2*np.pi)/(self.wav*1e6)
                            X[t, (2*c+1)*self.nwav:(2*c+2)*self.nwav] = -P[t, c]*np.real(Yfit[dit, c, :])/np.imag(self.visErr[dit, c, :])*(2*np.pi)/(self.wav*1e6)
                    else:
                        Y[0, 2*c*self.nwav:(2*c+1)*self.nwav] = np.real(Yres[dit, c, :])
                        Y[0, (2*c+1)*self.nwav:(2*c+2)*self.nwav] = np.imag(Yres[dit, c, :])
                        for t in range(ntel):
                            X[t, 2*c*self.nwav:(2*c+1)*self.nwav] = P[t, c]*np.imag(Yfit[dit, c, :])*(2*np.pi)/(self.wav*1e6)
                            X[t, (2*c+1)*self.nwav:(2*c+2)*self.nwav] = -P[t, c]*np.real(Yfit[dit, c, :])*(2*np.pi)/(self.wav*1e6)
                A = np.dot(Y, np.linalg.pinv(X))
                result = np.dot(A, X)
                for c in range(self.nchannel):
                    if (error == True):
                        Yresfit[dit, c, :] = result[0, 2*c*self.nwav:(2*c+1)*self.nwav]*np.real(self.visErr[dit, c, :]) + 1j*result[0, (2*c+1)*self.nwav:(2*c+2)*self.nwav]*np.imag(self.visErr[dit, c, :])
                    else:
                        Yresfit[dit, c, :] = result[0, 2*c*self.nwav:(2*c+1)*self.nwav] + 1j*result[0, (2*c+1)*self.nwav:(2*c+2)*self.nwav]                    
            Yres = self.visRef - Yfit - Yresfit
            if (error == True):
                sqrsum = np.nansum(np.real((self.visRef - Yfit -Yresfit))**2/np.real(self.visErr)**2 + np.imag((self.visRef - Yfit -Yresfit))**2/np.imag(self.visErr)**2)
            else:
                sqrsum = np.nansum(np.abs((self.visRef - Yfit -Yresfit))**2)
        if (fitMetrology == True):
            return (Yfit + Yresfit, wavelet, sqrsum)
        else:
            return (Yfit, wavelet, sqrsum)            

    def computeChi2Map(self, xvalues, yvalues, visInst = None, radec = False, useStarModel = False, fitMetrology = False):
        nxvalues = len(xvalues)
        nyvalues = len(yvalues)
        chi2Map = np.zeros([nxvalues, nyvalues])
        xBest = xvalues[0]
        yBest = yvalues[0]
        chi2Min = np.inf
        bestFit = None
        bestWavelet = None        
        pbar = patiencebar.Patiencebar(valmax = len(xvalues), title = "Calculating the chi-square map...")
        for k in range(nxvalues):
            pbar.update()
            for i in range(nyvalues):
                Yfit, wavelet, sqrsum = self.fitPlanetDitByDit(xvalues[k], yvalues[i], visInst = visInst, error = True, radec = radec, useStarModel = useStarModel, fitMetrology = fitMetrology)
                chi2Map[k, i] = sqrsum
                if sqrsum < chi2Min:
                    chi2Min = sqrsum
                    xBest = xvalues[k]
                    yBest = yvalues[i]
                    bestFit = Yfit
                    bestWavelet = wavelet                    
        self.fit = {"chi2Map": chi2Map, "chi2Min": chi2Min, "xvalus":xvalues, "yvalues": yvalues, "bestFit": bestFit, "xBest": xBest, "yBest": yBest, "radec": radec}
        self.visRefFit = bestFit
        self.visRefFitMean = np.nanmean(bestFit, axis = 0)
        self.wavelet = bestWavelet        
        if (radec == False):
            print("Best fit: "+str(xBest)+" deg, "+str(yBest)+" mas")
        else:
            print("Best fit: RA="+str(xBest)+" mas, DEC="+str(yBest)+" mas")            
        return None

    def plotChi2Map(self):
        plt.figure()
        plt.imshow(self.fit['chi2Map'].T, origin = "lower", extent = [np.min(self.fit['thetas']), np.max(self.fit['thetas']), np.min(self.fit['separations']), np.max(self.fit['separations'])])
        plt.colorbar()
        return None

    def plotFit(self):
        fig = plt.figure()
        reImPlot(self.freqMean, self.visRefMean, fig = fig)
        reImPlot(self.freqMean, self.visRefFitMean, fig = fig)
        return None

    def fitPlanetSpectrum(self, xvalue, yvalue, visInst = None, error = True, radec = False, useStarModel = False):
        """
        Fit a spectrum of the planet under the hypothesis that the planet is located at given separation and position angle (theta)
        visInst is the instrumental phase and amplitude
        """
        wav = self.wav
        if visInst is None:
            ampInst = np.ones(np.shape(self.visRef))
            phaseInst = np.zeros(np.shape(self.visRef))
        else:
            ampInst = np.abs(visInst)
            phaseInst = np.angle(visInst)
        if (radec == False):
            theta_rad = xvalue/360.0*2*np.pi
            this_u = (np.sin(theta_rad)*self.uCoord + np.cos(theta_rad)*self.vCoord)/1e7            
            wavelet = ampInst*np.exp(-1j*2*np.pi*this_u*1e7*separation/3600.0/360.0/1000.0*2*np.pi)
        else:
            this_u = (xvalue*self.uCoord + yvalue*self.vCoord)/1e7
            wavelet = ampInst*np.exp(-1j*2*np.pi*this_u*1e7/3600.0/360.0/1000.0*2*np.pi)
        if useStarModel == True:
            for c in range(self.nchannel):
                wavelet[:, c, :] = wavelet[:, c, :] - np.dot(np.dot(wavelet[:, c, :], np.linalg.pinv(self.visStarModel[c, :, :])), self.visStarModel[c, :, :])
        Yfit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        spectrum = np.zeros(self.nwav)
        spectrum_err = np.zeros(self.nwav)        
        # ignore all nan points in the fit by setting all them to 0
        visRef = np.copy(self.visRef)
        ind = np.where(np.isnan(visRef))
        visRef[ind] = 0
        wavelet[ind] = 0
        Y = np.zeros([self.ndit, self.nchannel, 2])
        X = np.zeros([self.ndit, self.nchannel, 2])
        for k in range(self.nwav):
            if (error == True):
                Y[:, :, 0] = np.real(visRef[:, :, k])/np.real(self.visErr[:, :, k])
                Y[:, :, 1] = np.imag(visRef[:, :, k])/np.imag(self.visErr[:, :, k])                
                X[:, :, 0] = np.real(wavelet[:, :, k])/np.real(self.visErr[:, :, k])
                X[:, :, 1] = np.imag(wavelet[:, :, k])/np.imag(self.visErr[:, :, k])
            else:
                Y[:, :, 0] = np.real(visRef[:, :, k])
                Y[:, :, 1] = np.imag(visRef[:, :, k])
                X[:, :, 0] = np.real(wavelet[:, :, k])
                X[:, :, 1] = np.imag(wavelet[:, :, k])
            Yflat = np.zeros([1, self.ndit*self.nchannel*2])
            Xflat = np.zeros([1, self.ndit*self.nchannel*2])
            Yflat[0, :] = Y.flatten()
            Xflat[0, :] = X.flatten()
            XflatInv = np.linalg.pinv(Xflat)
            A = np.dot(Yflat, XflatInv)            
            Aerr = np.dot(np.dot(XflatInv.T, np.eye(self.ndit*self.nchannel*2)), XflatInv)
            spectrum[k] = A[0, 0]
            spectrum_err[k] = Aerr[0, 0]**0.5
        Yfit = wavelet*spectrum[None, None, :]
        sqrsum = np.nansum(np.real(visRef - Yfit)**2/np.real(self.visErr)**2 + np.imag(visRef - Yfit)**2/np.imag(self.visErr)**2)
        self.visRefFitSpec = Yfit
        self.visRefFitSpecMean = np.mean(Yfit, axis = 0)
        return (Yfit, wavelet, sqrsum, spectrum, spectrum_err)

    def fitPlanetSpectrumByChannel(self, xvalue, yvalue, visInst = None, error = True, radec = False, useStarModel = False):
        """
        Fit a spectrum of the planet under the hypothesis that the planet is located at given separation and position angle (theta)
        visInst is the instrumental phase and amplitude
        """
        wav = self.wav
        if visInst is None:
            ampInst = np.ones(np.shape(self.visRef))
            phaseInst = np.zeros(np.shape(self.visRef))
        else:
            ampInst = np.abs(visInst)
            phaseInst = np.angle(visInst)
        if (radec == False):
            theta_rad = xvalue/360.0*2*np.pi
            this_u = (np.sin(theta_rad)*self.uCoord + np.cos(theta_rad)*self.vCoord)/1e7            
            wavelet = ampInst*np.exp(-1j*2*np.pi*this_u*1e7*separation/3600.0/360.0/1000.0*2*np.pi)
        else:
            this_u = (xvalue*self.uCoord + yvalue*self.vCoord)/1e7
            wavelet = ampInst*np.exp(-1j*2*np.pi*this_u*1e7/3600.0/360.0/1000.0*2*np.pi)
        if useStarModel == True:
            for c in range(self.nchannel):
                wavelet[:, c, :] = wavelet[:, c, :] - np.dot(np.dot(wavelet[:, c, :], np.linalg.pinv(self.visStarModel[c, :, :])), self.visStarModel[c, :, :])
        Yfit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        spectrum = np.zeros([self.nchannel, self.nwav])
        spectrum_err = np.zeros([self.nchannel, self.nwav])        
        # ignore all nan points in the fit by setting all them to 0
        visRef = np.copy(self.visRef)
        ind = np.where(np.isnan(visRef))
        visRef[ind] = 0
        wavelet[ind] = 0
        Y = np.zeros([self.ndit, 2])
        X = np.zeros([self.ndit, 2])
        Xflat = np.zeros([1, self.ndit*2])
        Yflat = np.zeros([1, self.ndit*2])
        for k in range(self.nwav):
            for c in range(self.nchannel):
                if (error == True):
                    Y[:, 0] = np.real(visRef[:, c, k])/np.real(self.visErr[:, c, k])
                    Y[:, 1] = np.imag(visRef[:, c, k])/np.imag(self.visErr[:, c, k])                
                    X[:, 0] = np.real(wavelet[:, c, k])/np.real(self.visErr[:, c, k])
                    X[:, 1] = np.imag(wavelet[:, c, k])/np.imag(self.visErr[:, c, k])
                else:
                    Y[:, 0] = np.real(visRef[:, c, k])
                    Y[:, 1] = np.imag(visRef[:, c, k])
                    X[:, 0] = np.real(wavelet[:, c, k])
                    X[:, 1] = np.imag(wavelet[:, c, k])
                Yflat[0, :] = Y.flatten()
                Xflat[0, :] = X.flatten()
                XflatInv = np.linalg.pinv(Xflat)
                A = np.dot(Yflat, XflatInv)
                Aerr = np.dot(np.dot(XflatInv.T, np.eye(self.ndit*2)), XflatInv)                
                spectrum[c, k] = A[0, 0]
                spectrum_err[c, k] = Aerr[0, 0]**0.5                
        Yfit = wavelet*spectrum[None, :, :]
        sqrsum = np.nansum(np.real(visRef - Yfit)**2/np.real(self.visErr)**2 + np.imag(visRef - Yfit)**2/np.imag(self.visErr)**2)
        self.visRefFitSpec = Yfit
        self.visRefFitSpecMean = np.mean(Yfit, axis = 0)        
        return (Yfit, wavelet, sqrsum, spectrum, spectrum_err)        
    

    def fitPlanetDitByDitSingleChannel(self, channel, xvalue, yvalue, phaseError = 0, visInst = None, error = True, spectrum = None, radec = False, useStarModel = False):
        """
        Fit each dit of a single channel using a model made of a wavelet corresponding to given position, and
        a potential metrology phase error
        visInst is the instrumental phase and amplitude
        You can also give a planet spectrum.
        """
        wav = self.wav
        if visInst is None:
            ampInst = np.ones(self.nwav)
            phaseInst = np.zeros(self.nwav)
        else:
            ampInst = np.abs(visInst)[channel, :]
            phaseInst = np.angle(visInst)[channel, :]
        if spectrum is None:
            spectrum = np.ones(self.nwav)
        if (radec == False):
            theta_rad = xvalue/360.0*2*np.pi
            this_u = (np.sin(theta_rad)*self.uCoord[:, channel, :] + np.cos(theta_rad)*self.vCoord[:, channel, :])/1e7            
            wavelet = spectrum[None, :]*ampInst*np.exp(-1j*2*np.pi*this_u*1e7*separation/3600.0/360.0/1000.0*2*np.pi)
        else:
            this_u = (xvalue*self.uCoord[:, channel, :] + yvalue*self.vCoord[:, channel, :])/1e7
            wavelet = spectrum[None, :]*ampInst*np.exp(-1j*2*np.pi*this_u*1e7/3600.0/360.0/1000.0*2*np.pi)
        # shift the wavelet
        wavelet = wavelet * np.exp(1j*phaseError*1.9e-6/self.wav)        
        if useStarModel == True:
            wavelet[:, :] = wavelet[:, :] - np.dot(np.dot(wavelet[:, :], np.linalg.pinv(self.visStarModel[channel, :, :])), self.visStarModel[channel, :, :])
        Yfit = np.zeros([self.ndit, self.nwav], 'complex')
        # ignore all nan points in the fit by setting all them to 0
        visRef = np.copy(self.visRef[:, channel, :])
        visErr = np.copy(self.visErr[:, channel, :])    
        ind = np.where(np.isnan(visRef))
        visRef[ind] = 0
        wavelet[ind] = 0
        for dit in range(self.ndit):
            Y = np.zeros([1, 2*self.nwav])
            X = np.zeros([1, 2*self.nwav])                        
            if (error == True):
                Y[0, 0:self.nwav] = np.real(visRef[dit, :])/np.real(visErr[dit, :])
                Y[0, self.nwav::] = np.imag(visRef[dit, :])/np.imag(visErr[dit, :])                
                X[0, 0:self.nwav] = np.real(wavelet[dit, :])/np.real(visErr[dit, :])
                X[0, self.nwav::] = np.imag(wavelet[dit, :])/np.imag(visErr[dit, :])
            else:
                Y[0, 0:self.nwav] = np.real(visRef[dit, :])
                Y[0, self.nwav::] = np.imag(visRef[dit, :])
                X[0, 0:self.nwav] = np.real(wavelet[dit, :])
                X[0, self.nwav::] = np.imag(wavelet[dit, :])
            A = np.dot(Y, np.linalg.pinv(X))
            result = np.dot(A, X)
            if (error == True):
                Yfit[dit, :] = result[0, 0:self.nwav]*np.real(visErr[dit, :]) + 1j*result[0, self.nwav::]*np.imag(visErr[dit, :])
            else:
                Yfit[dit, c, :] = result[0, 0:self.nwav] + 1j*result[0, self.nwav::]
        Yres = self.visRef[:, channel, :] - Yfit
        if (error == True):
            sqrsum = np.nansum(np.real((Yfit - visRef))**2/np.real(visErr)**2 + np.imag((Yfit - visRef))**2/np.imag(visErr)**2)
        else:
            sqrsum = np.nansum(np.abs((Yfit - visRef))**2)
        return (Yfit, sqrsum)

    def computeChi2MapPerChannel(self, xvalues, yvalues, visInst = None, radec = False, useStarModel = False, phaseErrors = None):
        """
        Calculate a chi2Map for each channel and combine them to find the best fit on all channels
        """
        if (phaseErrors is None):
            phaseErrors = np.array([0])
        if not(phaseErrors is None):
            if (phaseErrors[0] + phaseErrors[-1]) != 0:
                raise Exception("Phase error must be centered on 0!")
        if np.any(phaseErrors != np.linspace(phaseErrors[0], phaseErrors[-1], len(phaseErrors))):
                raise Exception("Phase error must be linearly spaced!")            
        nerr = len(phaseErrors)
        if (nerr%2 == 0):
            raise Exception("Phase error must have an odd number of points!")
        diffPhaseErrors = np.linspace(2*np.min(phaseErrors), 2*np.max(phaseErrors), 2*nerr-1)
        nxvalues = len(xvalues)
        nyvalues = len(yvalues)
        pbar = patiencebar.Patiencebar(valmax = self.nchannel, title = "Calculating the chi-square maps...")
        fit = {}
        allChi2Maps = np.zeros([self.nchannel, 2*nerr-1, nxvalues, nyvalues])
        superChi2Map = np.zeros([nxvalues, nyvalues])        
        for c in range(self.nchannel):
            pbar.update()            
            chi2Map = np.zeros([2*nerr-1, nxvalues, nyvalues])
            xBest = xvalues[0]
            yBest = yvalues[0]
            chi2Min = np.inf
            bestFit = None
            for j in range(2*nerr-1):
                for k in range(nxvalues):
                    for i in range(nyvalues):
                        Yfit, sqrsum = self.fitPlanetDitByDitSingleChannel(c, xvalues[k], yvalues[i], visInst = visInst, error = True, radec = radec, useStarModel = useStarModel, phaseError = diffPhaseErrors[j])
                        chi2Map[j, k, i] = sqrsum
                        if sqrsum < chi2Min:
                            chi2Min = sqrsum
                            xBest = xvalues[k]
                            yBest = yvalues[i]
                            errBest = diffPhaseErrors[j]
                            bestFit = Yfit
#            self.fit[str(c)] = {"chi2Map": np.min(chi2Map, axis = 0), "chi2Min": chi2Min, "xvalues":xvalues, "yvalues": yvalues, "bestFit": bestFit, "xBest": xBest, "yBest": yBest, "phaseErrorBest": errBest, "radec": radec}
            allChi2Maps[c, :, :, :] = chi2Map
        # build the complete chi2Map with all 3 supplementary dimensions for each error
        errorMap = np.zeros([nxvalues, nyvalues, 4])
        superChi2Map = np.zeros([nxvalues, nyvalues])+np.inf
        pbar = patiencebar.Patiencebar(valmax = nerr, title = "Combining the chi-square maps...")        
        for t1err in range(nerr):
            pbar.update()
            for t2err in range(nerr):
                for t3err in range(nerr):
                    for t4err in range(nerr):
                        simpleChi2Map = np.zeros([nxvalues, nyvalues])                                
                        dt43 = nerr-1+t4err-t3err
                        dt42 = nerr-1+t4err-t2err
                        dt41 = nerr-1+t4err-t1err
                        dt32 = nerr-1+t3err-t2err
                        dt31 = nerr-1+t3err-t1err
                        dt21 = nerr-1+t2err-t1err
                        channelErrors = [dt43, dt42, dt41, dt32, dt31, dt21]
                        for c in range(self.nchannel):
                            simpleChi2Map[:, :] = simpleChi2Map[:, :] + allChi2Maps[c, channelErrors[c], :, :]
                        for i in range(nxvalues):
                            for j in range(nyvalues):
                                if simpleChi2Map[i, j] < superChi2Map[i, j]:
                                    superChi2Map[i, j] = simpleChi2Map[i, j]
                                    errorMap[i, j, :] = np.array([t4err, t3err, t2err, t1err])
#        superChi2Map = np.min(np.min(np.min(np.min(completeChi2Map, axis = 0), axis = 0), axis = 0), axis = 0)
        chi2Min = np.min(superChi2Map)
        (i, j) = np.where(superChi2Map == chi2Min)
        (i, j) = (i[0], j[0])  
        t4 = int(errorMap[i, j, 0])
        t3 = int(errorMap[i, j, 1])
        t2 = int(errorMap[i, j, 2])
        t1 = int(errorMap[i, j, 3])
        dt43 = diffPhaseErrors[nerr-1+t4-t3]
        dt42 = diffPhaseErrors[nerr-1+t4-t2]
        dt41 = diffPhaseErrors[nerr-1+t4-t1]
        dt32 = diffPhaseErrors[nerr-1+t3-t2]
        dt31 = diffPhaseErrors[nerr-1+t3-t1]
        dt21 = diffPhaseErrors[nerr-1+t2-t1]
        channelErrors = [dt43, dt42, dt41, dt32, dt31, dt21]
        telErrors = [phaseErrors[t4], phaseErrors[t3], phaseErrors[t2], phaseErrors[t1]]
        xBest = xvalues[i]
        yBest = yvalues[j]
        bestFit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        for c in range(self.nchannel):
            Yfit, sqrsum = self.fitPlanetDitByDitSingleChannel(c, xvalues[i], yvalues[j], visInst = visInst, error = True, radec = radec, useStarModel = useStarModel, phaseError = channelErrors[c])
            bestFit[:, c, :] = Yfit
        self.fit = {"chi2Map": superChi2Map, "chi2Min": chi2Min, "xvalues":xvalues, "yvalues": yvalues, "bestFit": bestFit, "xBest": xvalues[i], "yBest": yvalues[j], "radec": radec, "channelErrors": channelErrors, "telescopeErrors": telErrors}
        self.visRefFit = bestFit
        self.visRefFitMean = np.nanmean(bestFit, axis = 0)
        if (radec == False):
            print("Best fit: "+str(xBest)+" deg, "+str(yBest)+" mas")
        else:
            print("Best fit: RA="+str(xBest)+" mas, DEC="+str(yBest)+" mas")            
        return None

