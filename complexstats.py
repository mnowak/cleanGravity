# -*- coding: utf-8 -*-
"""
Function used for complex variable statistics (covariance and 
pseudo-covariance, complex-normal law, etc.)
@author: mnowak
"""

import numpy as np

def adj(z):
    return z.T.conj()

def cov(z):
    """
    returns the covariance matrix of the complex set of vectors z, supposed to be ordered in columns.
    e.g. cov(z) will consider that each column of z is a realization of the random variable Z,
    and will return the corresponding covariance
    """
    n = np.shape(z)[1] # number of columns
    m = np.mean(z, axis = 1) # mean of the columns
    cov = np.dot(z-m[:, None], adj(z)-adj(m)[None, :])/(n-1)
    return cov

def pcov(z, axis = 0):
    """
    returns the pseud-covariance matrix of the complex set of vectors z, supposed to be ordered in columns
    e.g. cov(z, axis=1) will consider that each column of z is a realization of the random variable Z,
    and will return the corresponding pseudo-covariance
    """
    n = np.shape(z)[1] # number of columns
    m = np.mean(z, axis=1) # mean of the columns
    cov = np.dot(z-m[:, None], z.T-m.T[None, :])/(n-1)
    return cov

def conj_extended(z):
    """
    returns the conjugate-extended matrix (duplicate the matrix along 
    the first axis, and conugate the second half)
    """
    shape = list(np.shape(z))
    nrows = shape[0]
    shape[0] = 2*nrows
    z2 = np.zeros(shape, 'complex')
    z2[0:nrows] = z
    z2[nrows:] = z.conj()
    return z2


def extended_covariance(cov, pcov):
    """
    Return the extended covariance matrice (W Z // Z.H W*)
    from the covariance W and pseudo-covariance Z.
    """
    s_cov = cov.shape
    s_pcov = pcov.shape
    if (s_cov != s_pcov):
        raise Exception('Covariance and pseudo-covariance matrices must have the same shape!')
    if (s_cov[0] != s_cov[1]):
        raise Exception("Covariance matrix should be square! Shape ("+str(s_cov[0])+", "+str(s_cov[1])+") not valid!")
    n = s_cov[0]
    W2 = np.zeros([2*n, 2*n], 'complex')
    W2[0:n, 0:n] = cov
    W2[0:n, n:2*n] = pcov
    W2[n:2*n, 0:n] = adj(pcov)
    W2[n:2*n, n:2*n] = cov.conj()
    return W2

def reIm2modPhaseError(value, valueErr):
    """
    convert an error from real/imaginary to an error on modulus/phase
    not taking into account any possible covariance, and using a simple
    linearization assumption
    """
    mod, phase = np.abs(value), np.angle(value)
    modErr = np.sqrt(np.real(valueErr)**2*np.real(value)**2+np.imag(valueErr)**2*np.imag(value)**2)/mod
    phaseErr = np.sqrt(np.real(valueErr)**2*np.imag(value)**2+np.imag(valueErr)**2*np.real(value)**2)/mod**2
    return modErr, phaseErr

def modPhase2ReImError(value, modError, phaseError):
    """
    convert an error from mod/phase to real/imag
    not taking into account any possible covariance, and using a simple
    linearization assumption
    """
    mod, phase = np.abs(value), np.angle(value)
    realErr = np.sqrt(np.cos(phase)**2*modError**2+mod**2*np.sin(phase)**2*phaseError**2)
    imagErr = np.sqrt(np.sin(phase)**2*modError**2+mod**2*np.cos(phase)**2*phaseError**2)    
    return realErr+1j*imagErr
