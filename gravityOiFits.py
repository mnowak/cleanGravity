#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A class to load the data from a gravity files into Python objects to simplify access.
@author: mnowak
"""
import numpy as np
import astropy.io.fits as fits
from .fluxOiFits import FluxOiFits
from .visOiFits import VisOiFits
from .vis2OiFits import Vis2OiFits
from .visOiFitsDualfieldScivis import VisOiFitsDualfieldScivis
from .visOiFitsSinglefieldScivis import VisOiFitsSinglefieldScivis
from .t3OiFits import T3OiFits
from .gravityGlobals import *
import scipy.sparse, scipy.sparse.linalg

from datetime import datetime

class GravityOiFits(object):
    """
    Generic class which loads generic quantities (telescope names, baselines names, date, headers, etc.)
    This class does not load any actual visibility, flux, or closure data, but is subclassed by GravitySinglefieldAstrored,
    GravityDualfieldScivis, etc.
    
    Args:
      filename (str): name of the fits file to load

    Attributes:
      filename (str): copied from init
      header (astropy.fits): astropy object containing the fits header
      obj (str): name of the object from the header
      target (str): name of target from header (don't know the difference with object)
      sObjName (str): name of science object from header
      airmass (float): airmass value at beginning of obs
      parangle (float): paralactic angle (deg)
      seeing: seeing at beginning of obs (as)
      sObjX, sObjY (floats): coordinates of the science fiber with respect to FT fiber (ra/dec, in mas)
      dit (float): integration time for each dit (in s)
      datetime (datetime): python datetime object containing the date of obs extracted from the header
      time (str): datetime converted to ISO format
      mjd (float): time of obs in MJD. Extracted from header
      telescopeNames (str list): the names of the telescopes
      stationNames (str list): names of stations (same as telescope names for UTs, and positions of ATs)
      stationIndices (int list): indices of the stations in ESO system
    """
    def __init__(self, filename):
        self.filename = filename        
        self.header = fits.getheader(filename)
        # retrieve target data
        self.obj = self.header["OBJECT"]
        try: # sometime there is no target in the HEADER. Do not know why
            self.target = self.header["HIERARCH ESO OBS TARG NAME"]
        except:
            self.target = "Unknown"
        self.sObjName = self.header["HIERARCH ESO INS SOBJ NAME"]
        # retrieve observation parameters
        self.airmass = self.header["HIERARCH ESO ISS AIRM START"]
        self.parangle = self.header["HIERARCH ESO ISS PARANG START"]
        self.tau0 = self.header["HIERARCH ESO ISS AMBI TAU0 START"]
        self.seeing = self.header["HIERARCH ESO ISS AMBI FWHM START"]                                
        self.sObjX = self.header["HIERARCH ESO INS SOBJ X"]
        self.sObjY = self.header["HIERARCH ESO INS SOBJ Y"]
        self.dit = self.header["HIERARCH ESO DET2 SEQ1 DIT"]        
        self.datetime = datetime.strptime(self.header["DATE-OBS"], TIME_ISO_FORMAT)
        self.time = (self.datetime - datetime(1970, 1, 1, 0, 0, 0)).total_seconds()
        self.mjd = self.header["MJD-OBS"]
        # retrieve telescope names and station names
        self.stationNames = fits.getdata(filename, ARRAY_OI_NAME, 1).field("STA_NAME") 
        self.stationIndices = fits.getdata(filename, ARRAY_OI_NAME, 1).field("STA_INDEX")
        return None

    def getClosestOi(self, oiList, forceAfter = False, forceBefore = False):
        """
        Given a list of other ois, look at the datetimes, and return the closest one.
        Can also be forced to return an oi with a smaller datetime (forceBefore), or larger (forceAfter)
        """
        res = None
        deltas = np.array([(self.datetime - oi.datetime).total_seconds() for oi in oiList])
        if (forceBefore==True):
            if (len(np.where(deltas >= 0)) > 0): # return something only if there is at least on Oi before
                deltas[np.where(deltas < 0)] = np.inf # get rid of Oi after
                ind = np.argmin(deltas)
                res = oiList[ind]
        elif (forceAfter==True):
            if (len(np.where(deltas <= 0)) > 0): # return something only if there is at least on Oi after
                deltas[np.where(deltas > 0)] = -np.inf
                ind = np.argmax(deltas)
                res = oiList[ind]
        else:
            if (len(oiList) > 0):
                ind = np.argmin(np.abs(deltas))
                res = oiList[ind]
        return res
        
class GravityDualfieldAstrored(GravityOiFits):
    """
    A class used to load an astrored - dualfield file. The OI_FLUX and OI_VIS are loaded,
    and a metrology correction is applied to the visRef in the visOi. 

    Args:
      filename (str): name of the file to load
      extension (int): the extension used to load the data. 10 or 11 depending on polarization
      corrMet (str): the metrology correction to apply. Can be "none", "drs" or "sylvestre" (only applicable is sylvestre reduced the data). Default "none".
      corrDisp (str): can be "none", "drs" or "sylvestre" depending on how you want to correct the dispersion. Default is "none".

    Attributes:
      fluxOi: contain the data from the fluxOi (see fluxOi class)
      visOi: contain the data from the visOi (see visOi class)
      basenames (list of str): the names of the baselines, in same ordering as used for the visibilities in visOi (is basenames[k] corresponds to visData[:, k, :]) 
      nwav (int): number of wavelength pixels
      wav (float[nwav]): wavelength grid values (in m)
    """
    def __init__(self, filename, extension = None, corrDisp = "None", corrMet = "None", reflag = False):
        super(GravityDualfieldAstrored, self).__init__(filename)
        if extension is None:
            raise Exception("Please provide an extension number!")
        self.swap = (self.header["HIERARCH ESO INS SOBJ SWAP"].strip() == "YES")
        self.fluxOi = FluxOiFits(filename, extension = extension, reduction = "astrored", mode = "dualfield")
        self.fluxOi.scaleFlux(1.0/self.dit) # normalize flux to dit time
        self.visOi = VisOiFits(filename, extension = extension, reduction = "astrored", mode = "dualfield")
#        if reflag == True:
#            self.visOi.reflag = fits.getdata(self.filename,'REFLAG').field('REFLAG').reshape([self.visOi.ndit, self.visOi.nchannel, self.visOi.nwav])
#            self.visOi.flagPoints(np.where(self.visOi.reflag))
        self.visOi.scaleVisibilities(1.0/self.dit) # normalize visibilitied to dit time
        self.basenames = get_basenames(self.visOi.stationIndices, self.stationIndices, self.stationNames)
        self.telnames = get_telnames(self.fluxOi.stationIndices, self.stationIndices, self.stationNames)
        self.extension = extension        
        self.wav = self.fluxOi.wav
        self.nwav = self.fluxOi.nwav
        self.ndit = self.fluxOi.ndit
        # calculate the phase correction accounting for metrology and possibly dispersion
        phaseCorr = np.zeros([self.ndit, self.visOi.nchannel, self.nwav])
        if (corrMet.lower() == "drs"):
            phaseCorr = phaseCorr+self.getCorrMet()
        elif (corrMet.lower() == "sylvestre"):
            phaseCorr = phaseCorr+self.getCorrMetSylvestre()
        elif (corrMet.lower() == "none"):
            pass
        else:
            raise Exception("Unknown corrMet: {}".format(corrMet))
        if (corrDisp.lower() == "drs"):
            phaseCorr = phaseCorr+self.getCorrDisp()
        elif (corrDisp.lower() == "sylvestre"):
            phaseCorr = phaseCorr+self.getCorrDispSylvestre()
        elif (corrDisp.lower() == "none"):
            pass            
        else:
            raise Exception("Unknown corrDisp: {}".format(corrDisp))
        # apply the phase correction
        self.visOi.addPhase(phaseCorr)
        return None

    def removeDits(self, indices):
        """
        Remove a number of dits from the data. All dit-based attributes are affected, including covariance matrices.
        The ndit attribute is reduced accordingle.
        indices must be a list
        """        
        self.visOi.removeDits(indices)
        self.fluxOi.removeDits(indices)
        self.ndit = self.ndit - len(indices)
        return None

    def calculateIncoherentFlux(self): 
        """Calculate the incoherent flux incoming on each baseline and store it in the visOi"""
        fluxInc = np.zeros([self.ndit, self.visOi.nchannel, self.nwav])
        fluxIncErr = np.zeros([self.ndit, self.visOi.nchannel, self.nwav])        
        for c in range(self.visOi.nchannel):
            t1 = np.where(self.fluxOi.stationIndices == self.visOi.stationIndices[c, 0]) # telescope 1
            t2 = np.where(self.fluxOi.stationIndices == self.visOi.stationIndices[c, 1]) # telescope 2
            for dit in range(self.visOi.ndit):
                f1 = self.fluxOi.flux[dit, t1, :]
                f2 = self.fluxOi.flux[dit, t2, :]
                f1err = self.fluxOi.fluxErr[dit, t1, :]
                f2err = self.fluxOi.fluxErr[dit, t2, :]
                fluxInc[dit, c, :] = np.sqrt(f1*f2)
                fluxIncErr[dit, c, :] = 0.5*np.sqrt(f2/f1*(f1err**2)+f1/f2*(f2err**2))
        self.visOi.fluxInc = fluxInc
        self.visOi.fluxIncErr = fluxIncErr
    
    def calibrateWithFlux(self):
        """Calibrate visRef by dividing by the correct flux from the telescopes"""
        self.calculateIncoherentFlux()
        self.visOi.visRef = self.visOi.visRef/self.visOi.fluxInc
        for dit in range(self.visOi.ndit):
            for c in range(self.visOi.nchannel):
                self.visOi.visRefCov[dit, c] = self.visOi.visRefCov[dit, c].todense() # much faster if converting to dense matrix before
                self.visOi.visRefPcov[dit, c] = self.visOi.visRefPcov[dit, c].todense()                
                for w in range(self.visOi.nwav):
                    self.visOi.visRefCov[dit][c][w, :] = self.visOi.visRefCov[dit][c][w, :]/self.visOi.fluxInc[dit, c, w]
                    self.visOi.visRefCov[dit][c][:, w] = self.visOi.visRefCov[dit][c][:, w]/self.visOi.fluxInc[dit, c, w]
                    self.visOi.visRefPcov[dit][c][w, :] = self.visOi.visRefPcov[dit][c][w, :]/self.visOi.fluxInc[dit, c, w]
                    self.visOi.visRefPcov[dit][c][:, w] = self.visOi.visRefPcov[dit][c][:, w]/self.visOi.fluxInc[dit, c, w]                    
                self.visOi.visRefCov[dit, c] = scipy.sparse.csc_matrix(self.visOi.visRefCov[dit, c])
                self.visOi.visRefPcov[dit, c] = scipy.sparse.csc_matrix(self.visOi.visRefPcov[dit, c])                
        # if some point have nans, we need to flag them
        self.visOi.flag[np.where(self.visOi.fluxInc==0)] = True
        # unclear why, but sometimes the flux is negative, and the sqrt in calculateIncoherentFlux returns a Nan
        # we also need to flag those points
        self.visOi.flag[np.where(np.isnan(self.visOi.fluxInc))] = True
        return None

    def getClosures(self):
        """
        return closure in degrees
        """
        closures = np.dot(C, np.angle(self.visOi.visRef))
        closures = np.mod(closures, 2*np.pi)
        closures[np.where(closures>np.pi)] = closures[np.where(closures>np.pi)]-2*np.pi
        return np.rad2deg(closures.transpose(1,0,2)) # first dimension will be dit number

    def getBispectrum(self):
        """
        return bispectrum and error
        """
        bispectrum = np.zeros([self.visOi.ndit, 4, self.visOi.nwav], "complex")+1
        bispectrum_cov = np.zeros([self.visOi.ndit, 4, self.visOi.nwav], "complex")
        bispectrum_pcov = np.zeros([self.visOi.ndit, 4, self.visOi.nwav], "complex")
        visCov = np.array([[np.diag(self.visOi.visRefCov[dit, b].todense()) for b in range(self.visOi.nchannel)] for dit in range(self.visOi.ndit)])
        visPcov = np.array([[np.diag(self.visOi.visRefPcov[dit, b].todense()) for b in range(self.visOi.nchannel)] for dit in range(self.visOi.ndit)])        
        for c in range(4):
            for b in range(self.visOi.nchannel):
                if C[c, b] == 1:
                    bispectrum_cov[:, c, :] = bispectrum[:, c, :]*np.conj(bispectrum[:, c, :])*visCov[:, b, :] + \
                                              self.visOi.visRef[:, b, :]*np.conj(self.visOi.visRef[:, b, :])*bispectrum_cov[:, c, :]
                    bispectrum_pcov[:, c, :] = bispectrum[:, c, :]*bispectrum[:, c, :]*visPcov[:, b, :] + \
                                              self.visOi.visRef[:, b, :]*self.visOi.visRef[:, b, :]*bispectrum_pcov[:, c, :]
                    bispectrum[:, c, :] = bispectrum[:, c, :]*self.visOi.visRef[:, b, :]

                if C[c, b] == -1:
                    bispectrum_cov[:, c, :] = bispectrum[:, c, :]*np.conj(bispectrum[:, c, :])*np.conj(visCov[:, b, :]) + \
                                              self.visOi.visRef[:, b, :]*np.conj(self.visOi.visRef[:, b, :])*bispectrum_cov[:, c, :]
                    bispectrum_pcov[:, c, :] = bispectrum[:, c, :]*bispectrum[:, c, :]*np.conj(visPcov[:, b, :]) + \
                                              np.conj(self.visOi.visRef[:, b, :])*np.conj(self.visOi.visRef[:, b, :])*bispectrum_pcov[:, c, :]                    
                    bispectrum[:, c, :] = bispectrum[:, c, :]*np.conj(self.visOi.visRef[:, b, :])
                    
        return bispectrum, bispectrum_cov, bispectrum_pcov
    
    def getCorrMet(self):
        """
        Return the phase of the strange correction of the metrology apparently required
        """
        # matrix to link telescopes and baselines
        metCorrToPhaseCorr = np.array([[1, -1, 0, 0],
                                       [1, 0, -1, 0],
                                       [1, 0, 0, -1],
                                       [0, 1, -1, 0],
                                       [0, 1, 0, -1],
                                       [0, 0, 1, -1]])
        phaseCorr = -2*np.pi/self.wav*(np.dot(metCorrToPhaseCorr, (self.fluxOi.telFcCorr + self.fluxOi.fcCorr).T).T[:, :, None])
        self.phaseCorr = phaseCorr
        return phaseCorr

    def getCorrMetSylvestre(self):
        """The very strange correction used by sylvestre. Nobody knows what's in there."""    
        # matrix to link telescopes and baselines
        ### OLD VERSION ###
#        metCorrToPhaseCorr = np.array([[1, -1, 0, 0],
#                                       [1, 0, -1, 0],
#                                       [1, 0, 0, -1],
#                                       [0, 1, -1, 0],
#                                       [0, 1, 0, -1],
#                                       [0, 0, 1, -1]])                
#        lambdaLaser=self.header["HIERARCH ESO INS MLC WAVELENG"]/1e9        
#        piston2=fits.getdata(self.filename, 'SYLVESTRE').field('PISTON').reshape(self.fluxOi.ndit, self.fluxOi.nchannel)
#        metCorr=-piston2/(2*np.pi/lambdaLaser)
#        phaseCorr=2*np.pi/self.wav*np.dot(metCorrToPhaseCorr, metCorr.T).T[:, :, None]
        # another OLD VERSION #
        #phaseCorr=np.array([fits.getdata(self.filename,'SYLVESTRE').field('PISTON_B%i'%(i+1)).reshape((self.visOi.ndit,-1)) for i in range(self.visOi.nchannel)]).transpose((1,0,2))
        # MODERN VERSION
        phaseCorr  = fits.getdata(self.filename, 'OI_VIS', self.extension).field('PHASE_MET_TELFC').reshape(self.ndit, self.visOi.nchannel, -1)
        return -phaseCorr

    def getCorrDispSylvestre(self):
        # was updated and is now the same as drs version
        return -2*np.pi*self.visOi.opdDisp/self.visOi.wav           

    def getCorrDisp(self):
        """Return the phase of the correction of the visibilities for dispersion like the pipeline does. Should only be used with "astrored" data."""
        if self.visOi.reduction != "astrored":
            raise Exception('corrDisp should only be used on visOi with reduction astrored, and not with {}'.format(self.reduction))
        return -2*np.pi*self.visOi.opdDisp/self.visOi.wav           

    def computeMean(self):
        """Compute the meand of the fluxOi and visOi"""
        self.fluxOi.computeMean()
        self.visOi.computeMean()
        return None
    

class GravitySinglefieldAstrored(GravityOiFits):
    """
    A class used to load a astrored - singlefield file. The OI_FLUX and OI_VIS are loaded

    Args:
      filename (str): name of the file to load
      extension (int): the extension used to load the data. 10 or 11 depending on polarization
      corrMet (str): the metrology correction to apply. Can be "none", "drs" or "sylvestre" (only applicable is sylvestre reduced the data). Default "none".
      corrDisp (str): can be "none", "drs" or "sylvestre" depending on how you want to correct the dispersion. Default is "none".

    Attributes:
      fluxOi: contain the data from the fluxOi (see fluxOi class)
      visOi: contain the data from the visOi (see visOi class)
      basenames (list of str): the names of the baselines, in same ordering as used for the visibilities in visOi (is basenames[k] corresponds to visData[:, k, :]) 
      nwav (int): number of wavelength pixels
      wav (float[nwav]): wavelength grid values (in m)
    """
    def __init__(self, filename, extension = None, corrMet = "none", corrDisp = "none"):
        super(GravitySinglefieldAstrored, self).__init__(filename)        
        self.fluxOi = FluxOiFits(filename, extension = extension, reduction = 'astrored', mode = 'singlefield')
        self.visOi = VisOiFits(filename, extension = extension, reduction = 'astrored', mode = "singlefield")
        self.visOi.visAmp = np.abs(self.visOi.visData)
        #self.visOi.visAmpErr = ??        
        self.basenames = get_basenames(self.visOi.stationIndices, self.stationIndices, self.stationNames)
        self.telnames = get_telnames(self.fluxOi.stationIndices, self.stationIndices, self.stationNames)
        self.extension = extension
        self.wav = self.fluxOi.wav
        self.nwav = self.fluxOi.nwav
        self.ndit = self.fluxOi.ndit
        return None

    def computeMean(self):
        """Compute the meand of the fluxOi and visOi"""
        self.fluxOi.computeMean()
        self.visOi.computeMean()
        return None

    def calibrateWithFlux(self):
        """
        Calculate visAmp by dividing abs(visData) by the correct flux from the telescopes
        """
        for c in range(self.visOi.nchannel):
            t1 = np.where(self.stationIndices == self.visOi.stationIndices[c, 0]) # telescope 1
            t2 = np.where(self.stationIndices == self.visOi.stationIndices[c, 1]) # telescope 2
            for dit in range(self.visOi.ndit):
                f1 = self.fluxOi.flux[dit, 3-t1[0], :]
                f2 = self.fluxOi.flux[dit, 3-t2[0], :]
                self.visOi.visAmp[dit, c, :] = self.visOi.visAmp[dit, c, :]/np.sqrt(f1*f2*self.visOi.vFactor[dit, c, :])
                #self.visOi.visAmpErr = ??
        return None
    
class GravityDualfieldScivis(GravityOiFits):
    """
    A class used to load a scivis reduced - dualfield file. The OI_FLUX and OI_VIS are loaded, as well as the OI_T3
    The visOi comes with a bunch of methods specific to dualfield data

    Args:
      filename (str): name of the file to load
      extension (int): the extension used to load the data. 10 or 11 depending on polarization

    Attributes
      fluxOi: contain the data from the fluxOi (see fluxOi class)
      visOi: contain the data from the visOi (see visOiDualfieldScivis class)
      t3Oi: closure phases data (see t3Oi class)
      basenames (list of str): the names of the baselines, in same ordering as used for the visibilities in visOi (is basenames[k] corresponds to visData[:, k, :]) 
      closurena;es (list of str): the names of the closures, in same ordering as used for the data in t3Oi (is closurenames[k] corresponds to t3Phi[:, k, :]) 
      nwav (int): number of wavelength pixels
      wav (float[nwav]): wavelength grid values (in m)
    """
    def __init__(self, filename, extension = None):
        super(GravityDualfieldScivis, self).__init__(filename)
        self.extension = extension        
        self.swap = (self.header["HIERARCH ESO INS SOBJ SWAP"].strip() == "YES")        
        self.fluxOi = FluxOiFits(filename, extension = extension, reduction = "scivis", mode = 'dualfield')
        self.visOi = VisOiFitsDualfieldScivis(filename, extension = extension)
        self.t3Oi = T3OiFits(filename, extension = extension, reduction = 'scivis', mode = 'dualfield')
        self.basenames = get_basenames(self.visOi.stationIndices, self.stationIndices, self.stationNames)
        self.telnames = get_telnames(self.fluxOi.stationIndices, self.stationIndices, self.stationNames)                
        self.closurenames = get_closurenames(self.visOi.stationIndices, self.stationIndices, self.stationNames)                        
        self.wav = self.fluxOi.wav
        self.nwav = self.fluxOi.nwav

    def calibrateWithFlux(self): 
        """
        Calibrate visRef by dividing by the correct flux from the telescopes
        NOT SURE OF THE ORDERING OF THE TELESCOPES
        """
        for c in range(self.visOi.nchannel):
            t1 = np.where(self.stationIndices == self.visOi.stationIndices[c, 0]) # telescope 1
            t2 = np.where(self.stationIndices == self.visOi.stationIndices[c, 1]) # telescope 2
            for dit in range(self.visOi.ndit):
                f1 = self.fluxOi.flux[dit, 3-t1[0], :]
                f2 = self.fluxOi.flux[dit, 3-t2[0], :]
                self.visOi.visRef[dit, c, :] = (self.visOi.visRef[dit, c, :])/np.sqrt(f1*f2)
                #self.visOi.visAmpErr = ??               

    def computeMean(self):
        self.fluxOi.computeMean()
        self.visOi.computeMean()
        return None


class GravitySinglefieldScivis(GravityOiFits):
    """
    A class used to load a scivis reduced - singlefield file. The OI_FLUX and OI_VIS are loaded, as well as the OI_T3
    The visOi comes with a bunch of methods to fit data
    The visOi comes with a bunch of methods specific to dualfield data

    Args:
      filename (str): name of the file to load
      extension (int): the extension used to load the data. 10 or 11 depending on polarization

    Attributes
      fluxOi: contain the data from the fluxOi (see fluxOi class)
      visOi: contain the data from the visOi (see visOiDualfieldScivis class)
      vis2Oi: contain the data from the visOi (see vis2Oi class)
      t3Oi: closure phases data (see t3Oi class)
      basenames (list of str): the names of the baselines, in same ordering as used for the visibilities in visOi (is basenames[k] corresponds to visData[:, k, :]) 
      closurena;es (list of str): the names of the closures, in same ordering as used for the data in t3Oi (is closurenames[k] corresponds to t3Phi[:, k, :]) 
      nwav (int): number of wavelength pixels
      wav (float[nwav]): wavelength grid values (in m)
    """    
    def __init__(self, filename, extension = None):
        super(GravitySinglefieldScivis, self).__init__(filename)
        self.extension = extension        
        self.fluxOi = FluxOiFits(filename, extension = extension, reduction = 'scivis', mode = 'singlefield')
        self.visOi = VisOiFitsSinglefieldScivis(filename, extension = extension)
        self.vis2Oi = Vis2OiFits(filename, extension = extension, reduction = 'scivis', mode = 'singlefield')
        self.t3Oi = T3OiFits(filename, extension = extension, reduction = 'scivis', mode = 'singlefield')        
        self.basenames = get_basenames(self.visOi.stationIndices, self.stationIndices, self.stationNames)
        self.telnames = get_telnames(self.fluxOi.stationIndices, self.stationIndices, self.stationNames)                
        self.closurenames = get_closurenames(self.visOi.stationIndices, self.stationIndices, self.stationNames)                
        self.wav = self.fluxOi.wav
        self.nwav = self.fluxOi.nwav                

    def computeMean(self):
        self.fluxOi.computeMean()
        self.visOi.computeMean()
        return None

    def calibrateWithFlux(self): 
        """
        Calibrate visRef by dividing by the correct flux from the telescopes
        NOT SURE OF THE ORDERING OF THE TELESCOPES
        """
        for c in range(self.visOi.nchannel):
            t1 = np.where(self.stationIndices == self.visOi.stationIndices[c, 0]) # telescope 1
            t2 = np.where(self.stationIndices == self.visOi.stationIndices[c, 1]) # telescope 2
            for dit in range(self.visOi.ndit):
                f1 = self.fluxOi.flux[dit, t1, :]
                f2 = self.fluxOi.flux[dit, t2, :]
                self.visOi.visAmp[dit, c, :] = (self.visOi.visAmp[dit, c, :])/np.sqrt(f1*f2)
                #self.visOi.visAmpErr = ??                   


def get_telnames(telescopeIndices, stationIndices, stationNames): 
    """
    Given a list of stations names, and a list of corresponding 'indices' (i.e. stationName[k] has index stationIndices[k] in the ESO system,
    return the list of names for the stations given as indices of stations). 
    """   
    telescopes = []
    stationIndices = list(stationIndices)    
    for t in telescopeIndices:
        telescopes.append(stationNames[stationIndices.index(t)])
    return telescopes
    
def get_basenames(baselineStations, stationIndices, stationNames):
    """
    Given a list of stations names, and a list of corresponding 'indices' (i.e. stationName[k] has index stationIndices[k] in the ESO system,
    return the list of names for the baselines given as couple of indices (i, j). 
    """
    basenames = []
    stationIndices = list(stationIndices)
    for b in baselineStations:
        (i, j) = b
        s1 = stationNames[stationIndices.index(i)]
        s2 = stationNames[stationIndices.index(j)]
        basenames.append(s1+"-"+s2)
    return basenames


def get_closurenames(baselineStations, stationIndices, stationNames):
    """
    Don't remember how this works. 
    """
    closures = [C[k, :] for k in range(C.shape[0])]
    closurenames = []
    stationIndices = list(stationIndices)
    for c in closures:
        bs = np.where([c != 0])[1]
        tel = []
        for b in bs:
            i, j = baselineStations[b]
            s1 = stationNames[stationIndices.index(i)]
            s2 = stationNames[stationIndices.index(j)]
            if not(s1 in tel):
                tel.append(s1)
            if not(s2 in tel):
                tel.append(s2)                
        closurenames.append('-'.join(tel))
    return closurenames
