# -*- coding: utf-8 -*-
"""
A class to load the data from a gravity .astrored file T3 oiFits (closures)
@author: mnowak
"""
import numpy as np
import astropy.io.fits as fits
from .genericOiFits import GenericOiFits 
from .gravityGlobals import *

class T3OiFits(GenericOiFits):
    """
    Load the flux data from the T3 OI containing the closure phases.

    Args:
      filename (str): the name of the fits file from which to load the data
      extension (int): extension for the OI, used in calls to astropy.io.fits.getdata. Usually 9 or 10 depending on polarization
      reduction (str): Level of reduction can be 'astrored' or 'scivis'. Not used in practice
      mode (str): can be 'singlefield' or 'dualfield'. Not used in practice

    Attributes:
      u1, v1 (float[ndits, nchannels]): UV values of the first baselines of each closure (in m)
      u2, v2 (float[ndits, nchannels]): UV values of the second baselines of each closure (in m)
      u1Coord, v1Coord (float[ndits, nchannels, nwav]): U1 and V1 divided by lambda over the third dimension (rad^-1)
      u2Coord, v2Coord (float[ndits, nchannels, nwav]): U1 and V1 divided by lambda over the third dimension (rad^-1)
      t3Phi, t3PhiErr (float[[ndits, nchannels, nwav]): phase value of each closure phase, and associated error (in rad)
      t3Amp, t3AmpErr (float[[ndits, nchannels, nwav]): amplitude of each closure phase, and associated error
    """    
    def __init__(self, filename, extension = None, reduction = None, mode = None):        
        super(T3OiFits, self).__init__(filename = filename, oiname = T3_OI_NAME, nchannel = 4, reduction = reduction, extension = extension, mode = mode)
        self.oiname = T3_OI_NAME
        self.stationIndices = self.fits.field("STA_INDEX")        
        # baselines coordinates
        self.u1=self.fits.field("U1COORD").reshape(self.ndit, self.nchannel)
        self.v1=self.fits.field("V1COORD").reshape(self.ndit, self.nchannel)
        self.u2=self.fits.field("U2COORD").reshape(self.ndit, self.nchannel)
        self.v2=self.fits.field("V2COORD").reshape(self.ndit, self.nchannel)        
        self.u1Coord=self.u1[:, :, None]/self.wav
        self.v1Coord=self.v1[:, :, None]/self.wav
        self.u2Coord=self.u2[:, :, None]/self.wav
        self.v2Coord=self.v2[:, :, None]/self.wav
        self.t3Phi=self.loadData("T3PHI")
        self.t3PhiErr=self.loadData("T3PHIERR")
        self.t3Amp=self.loadData("T3AMP")
        self.t3AmpErr=self.loadData("T3AMPERR")        
        return None

    def closureModel(self, ra, dec, ratio):
        """
        Calculate and return the closure phase for a binary system of given separation and ratios
        as should be measured given the baselines UV coord of the OI
        
        Args:
          ra, dec (float): RA, DEC separation of the model binary (in mas)
          ration (float): contrast ratio between the two stars
        """
        ra_rad = ra/1000.0/3600.0/360.0*2*np.pi
        dec_rad = dec/1000.0/3600.0/360.0*2*np.pi
        u3Coord = self.u2Coord + self.u1Coord
        v3Coord = self.v2Coord + self.v1Coord
    
        c1 = np.cos(2*np.pi*(ra_rad*self.u1Coord+dec_rad*self.v1Coord))
        s1 = np.sin(2*np.pi*(ra_rad*self.u1Coord+dec_rad*self.v1Coord))
        
        c2 = np.cos(2*np.pi*(ra_rad*self.u2Coord+dec_rad*self.v2Coord))
        s2 = np.sin(2*np.pi*(ra_rad*self.u2Coord+dec_rad*self.v2Coord))
        
        c3 = np.cos(2*np.pi*(ra_rad*u3Coord+dec_rad*v3Coord))
        s3 = np.sin(2*np.pi*(ra_rad*u3Coord+dec_rad*v3Coord))
        
        t1 = np.arctan2(ratio*s1, (1+ratio*c1))
        t2 = np.arctan2(ratio*s2, (1+ratio*c2))
        t3 = np.arctan2(ratio*s3, (1+ratio*c3))

        return -(t1+t2-t3)*180.0/np.pi



                                        
