# -*- coding: utf-8 -*-
"""
A class to load the data from a gravity .astrored file visOiFits
@author: mnowak
"""
import numpy as np
import astropy.io.fits as fits
from .genericOiFits import GenericOiFits 
from .gravityGlobals import *

class Vis2OiFits(GenericOiFits):
    """
    A class used to store the data from the V2OI, containing the sqaured visibilities

    Args:
      filename (str): name of the file to load
      extension (int): the extension used to load the data. 10 or 11 depending on polarization
      reduction (str): reduction level can be "astrored" or "scivis". NOT IMPLEMENTED FOR ASTRORED
      mode (str): "dualfield" or "singlefield". In practicem V2 should only be used in singlefield

    Attributes:
      oiname (str): the name
      u, v (float[ndits, nchannels]): UV values for the baselines (in m)
      uCoord, vCoord (float[ndits, nchannels, nwav]): u and v from above divided by lambda over the third dimension (in rad^-1)
      freq (float[ndits, nchannels, nwav): (uCoord**2+vCoord**2)**0.5
      vis2, vis2Err (float[ndits, nchannels, nwav]): squared visibilities
    """
    def __init__(self, filename, extension = None, reduction = None, mode = None):
        super(Vis2OiFits, self).__init__(filename = filename, oiname = VIS2_OI_NAME, nchannel = 6, reduction = reduction, extension = extension, mode = "singlefield")
        self.oiname = VIS2_OI_NAME
        self.stationIndices = self.fits.field("STA_INDEX")        
        self.u=self.fits.field("UCOORD").reshape(self.ndit, self.nchannel)
        self.v=self.fits.field("VCOORD").reshape(self.ndit, self.nchannel)
        self.uCoord=self.u[:, :, None]/self.wav
        self.vCoord=self.v[:, :, None]/self.wav
        self.freq = np.abs(self.uCoord + 1j*self.vCoord)
        if reduction == 'scivis':
            self.vis2 = self.loadData("VIS2DATA")
            self.vis2Err = self.loadData("VIS2ERR")
        return None

    def getOpd(self, ra, dec):
        """
        return the list of OPDs calculated for a source at given ra/dec (in mas)
        for each u v coordinate of each channel and each dit
        """
        ra_rad = ra/1000.0/3600.0/360.0*2*np.pi
        dec_rad = dec/1000.0/3600.0/360.0*2*np.pi        
        return (self.u*ra_rad + self.v*dec_rad)*1e6
