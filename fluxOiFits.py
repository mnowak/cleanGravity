# -*- coding: utf-8 -*-
"""
A class to load the data from a gravity .astrored file fluxOiFits
@author: mnowak
"""
import numpy as np
import astropy.io.fits as fits
from .genericOiFits import GenericOiFits 
from .gravityGlobals import *

class FluxOiFits(GenericOiFits):
    """
    Load the flux data from the flux OI.

    Args:
      filename (str): the name of the fits file from which to load the data
      extension (int): extension for the OI, used in calls to astropy.io.fits.getdata. Usually 9 or 10 depending on polarization
      reduction (str): Level of reduction can be 'astrored' or 'scivis'.
      mode (str): can be 'singlefield' or 'dualfield'. In dualfield with reduction astrored, the telescope-based metrology correction terms are loaded from flux OI.
    
    Attributes:
      flux, fluxErr (float[ndits, nchannels, nwav]): flux for each dit, each telescope, each wavelength pixel. And associated error. (in ADU)
      telFcCorr (float[ndits, nchannels]): the telescope based metrology correction of the phase (in rad) - only in dualfield/astrored. See GRAVITY pipeline doc.
      fcCorr (float[ndits, nchannels]): another part of the telescope based metrology correction of the phase (in rad) - only in dualfield/astrored. See GRAVITY pipeline doc.
    """
    def __init__(self, filename, reduction = None, extension = None, mode = None):
        # generic stuff. 4 channels because 4 telescopes
        super(FluxOiFits, self).__init__(filename = filename, oiname = FLUX_OI_NAME, nchannel = 4, reduction = reduction, extension = extension, mode = mode)
        self.stationIndices = self.fits.field("STA_INDEX")[0:self.nchannel]        
        # load proper flux data
        self.flux = self.loadData('FLUX')
        self.fluxErr = self.loadData('FLUXERR')
        # check mode/reduction level. In most cqses, nothing needs to be done
        if mode == 'singlefield':        
            if reduction == 'astrored': # for astrored/dualfield we need to load the telescope based metrology 
                self.telFcCorr = self.fits.field('OPD_MET_TELFC_MCORR').reshape(self.ndit, self.nchannel)
                self.fcCorr = self.fits.field('OPD_MET_FC_CORR').reshape(self.ndit, self.nchannel)
            elif reduction == 'scivis':
                pass
            else:
                raise Exception("Unknown reduction level: "+str(reduction)+"!")
        elif mode == 'dualfield':            
            if reduction == 'astrored': # for astrored/dualfield we need to load the telescope based metrology 
                self.telFcCorr = self.fits.field('OPD_MET_TELFC_MCORR').reshape(self.ndit, self.nchannel)
                self.fcCorr = self.fits.field('OPD_MET_FC_CORR').reshape(self.ndit, self.nchannel)
            elif reduction == 'scivis':
                pass
            else:
                raise Exception("Unknown reduction level: "+str(reduction)+"!")            
        else:
            raise Exception("Unknown mode: "+str(mode)+"!")
        return None

    def removeDits(self, indices):
        """
        Remove a number of dits from the data. All dit-based attributes are affected, including covariance matrices.
        The ndit attribute is reduced accordingle.
        indices must be a list
        """
        dits_to_keep = [dit for dit in range(self.ndit) if not(dit in indices)]
        if self.reduction != "astrored":
            raise Exception("Dit filtering is only available for astrored data, not {}".format(self.reduction))
        self.flux = self.flux[dits_to_keep]
        self.fluxErr = self.fluxErr[dits_to_keep]
        self.telFcCorr = self.telFcCorr[dits_to_keep]
        self.fcCorr = self.fcCorr[dits_to_keep]
        self.ndit = len(dits_to_keep)
        return None

    def computeMean(self): 
        """Calculate the mean of flux and associated fluxErr and store them in fluxMean and fluxMeanErr attributes"""
        self.fluxMean = np.nanmean(self.flux, axis = 0)
        self.fluxErrMean = np.sqrt(np.nanmean(self.fluxErr**2, axis = 0))
        return None

    def scaleFlux(self, factor):
        """Multiply flux and associated errors by a scale factor"""
        self.flux = self.flux*factor
        self.fluxErr = self.fluxErr*factor
        return None
