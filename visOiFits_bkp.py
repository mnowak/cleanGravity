#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A class to load the data from a gravity .astrored file visOiFits
@author: mnowak
"""
import numpy as np
import astropy.io.fits as fits
import patiencebar
from .genericOiFits import GenericOiFits 
import scipy.interpolate
from .gravityGlobals import *

class VisOiFits(GenericOiFits):
    """
    This class is used to gather and manipulate data from the VIS oi in gravity files. It can be used
    either with reduction = "astrored", or "scivis" and mode = 'singlefield' or 'dualfield'
    """
    def __init__(self, filename, extension = None, mode = None, reduction = None):
        super(VisOiFits, self).__init__(filename = filename, oiname = VIS_OI_NAME, nchannel = 6, reduction = reduction, extension = extension)
        self.oiname = VIS_OI_NAME
        self.stationIndices = self.fits.field("STA_INDEX")[0:self.nchannel]
        self.u=self.fits.field("UCOORD").reshape(self.ndit, self.nchannel)
        self.v=self.fits.field("VCOORD").reshape(self.ndit, self.nchannel)
        self.uCoord=self.u[:, :, None]/self.wav
        self.vCoord=self.v[:, :, None]/self.wav
        self.freq = np.abs(self.uCoord + 1j*self.vCoord)
        self.visData= self.loadData("VISDATA")/self.dit
        self.visErr= self.loadData("VISERR")/self.dit
        if mode == "singlefield":
            if reduction == "astrored":
                self.phaseRef = self.loadData("PHASE_REF")
                self.opdDisp = self.loadData("OPD_DISP")            
                self.gDelayDisp = self.fits.field("GDELAY_DISP").reshape(self.ndit, self.nchannel)
                self.phaseDisp = self.loadData("PHASE_DISP")
                self.visRef = self.visData * np.exp(1j*self.phaseRef) * np.exp(-1j*2*np.pi*self.opdDisp/self.wav) # phaseRef correct for FT zero point; opdDisp correct for dispersion                
            elif reduction == "scivis":
                self.visAmp = self.loadData("VISAMP")
                self.visPhi = self.loadData("VISPHI")
                self.visAmpErr = self.loadData("VISAMPERR")
                self.visPhiErr = self.loadData("VISPHIERR")                        
                self.visRef = self.visData
            else:
                raise Exception("Unknown reduction level: "+str(reduction)+"!")
        elif mode == "dualfield":
            if reduction == "astrored":
                self.phaseRef = self.loadData("PHASE_REF")
                self.opdDisp = self.loadData("OPD_DISP")            
                self.gDelayDisp = self.fits.field("GDELAY_DISP").reshape(self.ndit, self.nchannel)
                self.phaseDisp = self.loadData("PHASE_DISP")
                self.visRef = self.visData * np.exp(1j*self.phaseRef) * np.exp(-1j*2*np.pi*self.opdDisp/self.wav) # phaseRef correct for FT zero point; opdDisp correct for dispersion
                self.visRefFit = None
                self.visRefFitSpec = None                
            elif reduction == "scivis":
                raise Exception("Reduction level "+str(reduction)+" not implemented for mode "+str(mode)+"!")
            else:
                raise Exception("Unknown reduction level: "+str(reduction)+"!")
        else:
            raise Exception("Unknown mode: "+str(mode)+"!")
        return None
                                
    def _getSerie(self, serie, dit = -1):
        res = None
        if serie == 'visRef':
            res = self.visRef
        if not(res is None):
            if dit == -1:
                res = res[:, :, :]
            else:
                res = res[dit, :, :]                            
        return res

    def getOpd(self, ra, dec):
        """
        return the list of OPDs calculated for a source at given ra/dec (in arcsec)
        for each u v coordinate of each channel and each dit
        """
        ra_rad = ra/1000.0/3600.0/360.0*2*np.pi
        dec_rad = dec/1000.0/3600.0/360.0*2*np.pi        
        return (self.u*ra_rad + self.v*dec_rad)*1e6

    def computeMean(self):
        """
        Calculate and store the mean of each attribute dataset
        """
        self.uCoordMean = np.nanmean(self.uCoord, axis = 0)
        self.vCoordMean = np.nanmean(self.vCoord, axis = 0)        
        self.freqMean = np.abs(self.uCoordMean + 1j*self.vCoordMean)
        self.visDataMean= np.nanmean(self.visData, axis = 0)
        self.visErrMean= np.sqrt(np.nansum(self.visErr**2, axis = 0))/self.ndit
        if self.reduction == "astrored":
            self.phaseRefMean = np.nanmean(self.phaseRef, axis = 0)
            self.opdDispMean = np.nanmean(self.opdDisp, axis = 0)
            self.gDelayDispMean = np.nanmean(self.gDelayDisp, axis = 0)
            self.phaseDispMean = np.nanmean(self.phaseDisp, axis = 0)
        if (self.reduction == "dualscivis"):
            self.visAmpMean = np.mean(self.visAmp, axis = 0)
            self.visPhiMean = np.mean(self.visPhi, axis = 0)
            self.visAmpMeanErr = np.mean(self.visAmpErr, axis = 0)
            self.visPhiMeanErr = np.mean(self.visPhiErr, axis = 0)                                        
        self.visRefMean = np.nanmean(self.visRef, axis = 0)
        if not(self.visRefFit is None):
            self.visRefFitMean = np.nanmean(self.visRefFit, axis = 0)
        if not(self.visRefFitSpec is None):
            self.visRefFitSpecMean = np.nanmean(self.visRefFitSpec, axis = 0)        
        return None

    def addPhase(self, phase):
        """
        Add the given phase (function of wavelength and baseline) to the current visRef. Also update the error accordingly.
        """
        self.visRef = self.visRef * np.exp(1j*phase)
        if not(self.visRefMean is None):
            self.visRefMean = np.mean(self.visRef, axis = 0)
        if not(self.visRefFit is None):
            self.visRefFit = self.visRefFit * np.exp(1j*phase)
        if not(self.visRefFitSpec is None):
            self.visRefFitSpec = self.visRefFitSpec * np.exp(1j*phase)
        self.visErr = np.sqrt( np.cos(phase)**2*np.real(self.visErr)**2+np.sin(phase)**2*np.imag(self.visErr)**2 ) \
                      +1j*np.sqrt( np.sin(phase)**2*np.real(self.visErr)**2+np.cos(phase)**2*np.imag(self.visErr)**2 )
        return None

    def recenterPhase(self, xvalue, yvalue, radec = False):
        """
        Recenter visRef and visRefError on to the given position by shifting the phases
        """
        if (radec == False):
            theta_rad = xvalue/360.0*2*np.pi
            this_u = (np.sin(theta_rad)*self.uCoord + np.cos(theta_rad)*self.vCoord)/1e7            
            phase = 2*np.pi*this_u*1e7*separation/3600.0/360.0/1000.0*2*np.pi
        else:
            this_u = (xvalue*self.uCoord + yvalue*self.vCoord)/1e7
            phase = 2*np.pi*this_u*1e7/3600.0/360.0/1000.0*2*np.pi
        self.addPhase(phase)
        return None
    
    def fitStarDitByDit(self, visStar, order = 2, ra = 0, dec = 0, saturation = None):
        """
        Fit a star model to the visRef. The star model is the given visStar multiplied by a polynomial in
        wavelength of the given order.
        """
        # create the model for the star
        ampStar = np.abs(visStar)
        phaseStar = np.angle(visStar)
        X = np.zeros([self.nchannel, 2*(order+1), self.nwav], 'complex')
#        this_u = np.mean((ra*self.uCoord + dec*self.vCoord)/1e7, axis = 0)
#        phaseFactor = np.exp(-1j*2*np.pi*this_u*1e7/3600.0/360.0/1000.0*2*np.pi)
        for c in range(self.nchannel):
            for k in range(order+1):
                X[c, k, :] = ampStar[c, :]*(1e7*(self.wav - np.mean(self.wav)))**k
#                X[c, k, :] = ampStar[c, :]*(1.9e-6/self.wav)**k
            for k in range(order+1):
#                X[c, order+1+k, :] = ampStar[c, :]*(1e7*(self.wav - np.mean(self.wav)))**k * phaseFactor[c, :]
#                X[c, order+1+k, :] = 1*(1.9e-6/self.wav)**k*0
                if (saturation == "squared"):
                    X[c, order+1+k, :] = (ampStar[c, :]**2)*(1.9e-6/self.wav)**k
                if (saturation == "constant"):
                    X[c, order+1+k, :] = 1#*(1e7*(self.wav - np.mean(self.wav)))**k
                else:
                    X[c, order+1+k, :] = 0
        # store the model as is
        self.visStarModel = X
        # prepare the fit
        Yfit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        coeffs = np.zeros([self.ndit, self.nchannel, 2*(order+1)], 'complex')
        # star to fit, dit by dit, channel by channel
        for dit in range(self.ndit):    
            for c in range(self.nchannel):
                Y = self.visRef[dit, c, :]
                # ignore all nan points in the fit by setting all them to 0
                X = np.copy(self.visStarModel[c, :, :])
                ind = np.where(np.isnan(Y))
                Y[ind] = 0
                for k in range(order+1):
                    X[k, ind] = 0
                A = np.dot(Y, np.linalg.pinv(X))
                coeffs[dit, c, :] = A
                Yfit[dit, c, :] = np.dot(A, X)
                Yfit[dit, c, ind] = float('nan') + float('nan')*1j
        self.coeffs = coeffs
        # store everything for plotting later on
        self.visStar = Yfit
        self.visStarMean = np.nanmean(Yfit, axis = 0)
        Yres = self.visRef - Yfit
        self.visRef = Yres    
        self.visRefMean = np.nanmean(Yres, axis = 0)        
        return None
                             
    def fitOpdDitByDit(self, opdValue, channel = None, visInst = None, error = True, spectrum = None, useStarModel = False, simultaneousStarFit = False, starOrder = 1, starRa = 0, starDec = 0):
        """
        Fit each dit using a model made of a given spectrum multiplied by the visInst amplitude
        and a wavelet corresponding to given opd
        OPD given in micrometers
        It is also possible to simultaneously fit the star model with the opd by putting the simultaneousStarFit to True
        """
        # useStarModel and simultaneousStarFit cannot be both true
        if ((simultaneousStarFit==True) and (useStarModel == True)):
            raise Exception("simultaneousStarFit and useStarModel keyword arguments cannot be both set to 'True'")
        if (channel is None):
            raise Exception("Channel keyword argument cannot be 'None'!")
        wav = self.wav
        ampInst = np.ones([self.ndit, self.nwav])
        phaseInst = np.ones([self.ndit, self.nwav])
        # if visInst is given, use it as a base shape for the amplitude and phase
        if not(visInst is None):
            ampInst = ampInst*np.abs(visInst)[None, :]
            phaseInst = phaseInst*np.angle(visInst)[None, :]
        # if spectrum is given, also use it to modulate the expected amplitude, but else, set it to all ones
        if spectrum is None:
            spectrum = np.ones(self.nwav)
        wavelet = spectrum[None, :]*ampInst*np.exp(-1j*2*np.pi/(wav*1e6)*opdValue)
        # if user requested to use a pre-calculated starModel, check is this model is present, and if yes, project
        # the wavelet model onto the axis orthogonal to this starModel
        if useStarModel == True:
            wavelet = wavelet - np.dot(np.dot(wavelet, np.linalg.pinv(self.visStarModel[channel, :, :])), self.visStarModel[channel, :, :])
        Yfit = np.zeros([self.ndit, self.nwav], 'complex')
        YfitPlanet = np.zeros([self.ndit, self.nwav], 'complex')        
        # ignore all nan points in the fit by setting all them to 0
        visRef = np.copy(self.visRef[:, channel, :])
        ind = np.where(np.isnan(visRef))
        visRef[ind] = 0
        wavelet[ind] = 0
        sqrsums = np.zeros([self.ndit])
        for dit in range(self.ndit):
            # prepare the matrices for linear fit Y=AX
            Y = np.zeros([1, 2*self.nwav])            
            if (simultaneousStarFit == True):
                X = np.zeros([2*(starOrder+1)+1, 2*self.nwav])
            else:
                X = np.zeros([1, 2*self.nwav])                                        
            if (error == True):
                Y[0, 0:self.nwav] = np.real(visRef[dit, :])/np.real(self.visErr[dit, channel, :])
                Y[0, self.nwav:2*self.nwav] = np.imag(visRef[dit, :])/np.imag(self.visErr[dit, channel, :])
                if (simultaneousStarFit == True): 
                    X[0, 0:self.nwav] = np.real(wavelet[dit, :])/np.real(self.visErr[dit, channel, :])
                    X[0, self.nwav:2*self.nwav] = np.imag(wavelet[dit, :])/np.imag(self.visErr[dit, channel, :])
                    for k in range(starOrder+1):
                        X[2*k+1, 0:self.nwav] = ampInst[dit, :]*(1e7*(self.wav - np.mean(self.wav)))**k/np.real(self.visErr[dit, channel, :])
                        X[2*k+1, self.nwav:2*self.nwav] = 0
                        X[2*k+2, 0:self.nwav] = 0
                        X[2*k+2, self.nwav:2*self.nwav] = ampInst[dit, :]*(1e7*(self.wav - np.mean(self.wav)))**k/np.imag(self.visErr[dit, channel, :])
                else:
                    X[0, 0:self.nwav] = np.real(wavelet[dit, :])/np.real(self.visErr[dit, channel, :])
                    X[0, self.nwav:2*self.nwav] = np.imag(wavelet[dit, :])/np.imag(self.visErr[dit, channel, :])                    
            else:
                Y[0, 0:self.nwav] = np.real(visRef[dit, :])
                Y[0, self.nwav:2*self.nwav] = np.imag(visRef[dit, :])                
                if (simultaneousStarFit == True): 
                    X[0, 0:self.nwav] = np.real(wavelet[dit, :])
                    X[0, self.nwav:2*self.nwav] = np.imag(wavelet[dit, :])
                    for k in range(starOrder+1):
                        X[2*k+1, 0:self.nwav] = ampInst[dit, :]*(1e7*(self.wav - np.mean(self.wav)))**k
                        X[2*k+1, self.nwav:2*self.nwav] = 0
                        X[2*k+2, 0:self.nwav] = 0
                        X[2*k+2, self.nwav:2*self.nwav] = ampInst[dit, :]*(1e7*(self.wav - np.mean(self.wav)))**k
                else:
                    X[0, 0:self.nwav] = np.real(wavelet[dit, :])
                    X[0, self.nwav:2*self.nwav] = np.imag(wavelet[dit, :])
            A = np.dot(Y, np.linalg.pinv(X))
            result = np.dot(A, X)
            if (error == True):
                Yfit[dit, :] = result[0, 0:self.nwav]*np.real(self.visErr[dit, channel, :]) + 1j*result[0, self.nwav:2*self.nwav]*np.imag(self.visErr[dit, channel, :])
                YfitPlanet[dit, :] = A[0, 0]*X[0, 0:self.nwav]*np.real(self.visErr[dit, channel, :]) + 1j*A[0, 0]*X[0, self.nwav:2*self.nwav]*np.imag(self.visErr[dit, channel, :])
            else:
                Yfit[dit, :] = result[0, 0:self.nwav] + 1j*result[0, self.nwav:2*self.nwav]
                YfitPlanet[dit, :] = A[0, 0]*X[0, 0:self.nwav] + 1j*A[0, 0]*X[0, self.nwav:2*self.nwav]                                    
            Yres = self.visRef[dit, channel, :] - Yfit[dit, :]
            if (error == True):
                sqrsums[dit] = np.nansum(np.real(Yres)**2/np.real(self.visErr[dit, channel, :])**2 + np.imag(Yres)**2/np.imag(self.visErr[dit, channel, :])**2)
            else:
                sqrsums[dit] = np.nansum(np.real(Yres)**2 + np.imag(Yres)**2)
        return (Yfit, YfitPlanet, wavelet, sqrsums)

    
    def computeOpdChi2Maps(self, opdValues, visInst = None, useStarModel = False, simultaneousStarFit = False, starOrder = 0, starRa = 0, starDec = 0):
        nvalues = np.shape(opdValues)[1]
        chi2Maps = np.zeros([self.ndit, self.nchannel, nvalues]) # will contain the 1D chi-square for map for each channel and each dit
        chi2Mins = np.zeros([self.ndit, self.nchannel])+np.inf # will contain the best chi2
        opdBests = np.zeros([self.ndit, self.nchannel])+np.inf # will contain the opd corresponding to the best chi2
        bestFit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex') # will be filled with the best fits for each channel and each dit
        bestWavelet = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        pbar = patiencebar.Patiencebar(valmax = nvalues, title = "Calculating the chi-square map...")
        for k in range(nvalues):
            pbar.update()
            for c in range(self.nchannel):
                # if visInst is given, extract the correct channel
                if not(visInst is None):
                    thisVisInst = visInst[c, :]
                else:
                    thisVisInst = None
                Yfit, YfitPlanet, wavelet, sqrsums = self.fitOpdDitByDit(opdValues[c, k], channel = c, visInst = thisVisInst, error = True, useStarModel = useStarModel, simultaneousStarFit = simultaneousStarFit, starOrder = starOrder, starRa = starRa, starDec = starDec)
                chi2Maps[:, c, k] = sqrsums
                for dit in range(self.ndit):
                    if sqrsums[dit] < chi2Mins[dit, c]:
                        chi2Mins[dit, c] = sqrsums[dit]
                        opdBests[dit, c] = opdValues[c, k]
                        bestFit[dit, c, :] = Yfit[dit, :]
                        bestWavelet[dit, c, :] = wavelet[dit, :]                                    
        self.opdfit = {"chi2Maps": chi2Maps, "chi2Mins": chi2Mins, "opdValues":opdValues, "bestFit": bestFit, "opdBests": opdBests}
        self.visRefOpdFit = bestFit
        self.visRefOpdFitMean = np.nanmean(bestFit, axis = 0)
        self.opdWavelet = bestWavelet        
        return None

    def fitPlanetDitByDit(self, xvalue, yvalue, error = True, visInst = None, useStarModel = False, simultaneousStarFit = False, starOrder = 0):
        Yfit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        YfitPlanet = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')        
        wavelet = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        sqrsum = np.inf
        for c in range(self.nchannel):
            for dit in range(self.ndit):
                opd = self.getOpd(xvalue, yvalue)[dit, c]
                if not(visInst is None):
                    thisVisInst = visInst[c, :]                    
                else:
                    thisVisInst = None
                (channelYfit, channelYfitPlanet, channelWavelet, channelSqrsums) = self.fitOpdDitByDit(opd, channel = c, visInst = thisVisInst, useStarModel = useStarModel, error = error, simultaneousStarFit = simultaneousStarFit, starOrder = starOrder)
                Yfit[dit, c, :] = channelYfit[dit, :]
                YfitPlanet[dit, c, :] = channelYfitPlanet[dit, :]                
                wavelet[dit, c, :] = channelWavelet[dit, :]
                sqrsum = sqrsum + np.nansum(channelSqrsums)
        return Yfit, YfitPlanet, wavelet, sqrsum

    def computeChi2Map(self, xValues, yValues, nopds = None, visInst = None, recomputeOpdChi2Maps = True, useStarModel = False, simultaneousStarFit = False, starOrder = 0, starRa = 0, starDec = 0):
        """
        Use the opdChi2Map calculation to create a RA/DEC chi2 map, and extract the best fit in terms of RA/DEC from it
        RA and DEC values must be given in mas
        """
        nx = len(xValues)
        ny = len(yValues)
        if nopds is None:
            nopds = np.max([nx, ny])
        opds = np.zeros([self.ndit, self.nchannel, nx, ny])
        for i in range(nx):
            for j in range(ny):
                opds[:, :, i, j] = self.getOpd(xValues[i], yValues[j])
        if recomputeOpdChi2Maps:
            opdValues = np.zeros([self.nchannel, nopds])
            for c in range(self.nchannel):
                opdValues[c, :] = np.linspace(np.min(opds[:, c, :, :])-1, np.max(opds[:, c, :, :])+1, nopds)
            self.computeOpdChi2Maps(opdValues, visInst = visInst, useStarModel = useStarModel, simultaneousStarFit = simultaneousStarFit, starOrder = starOrder, starRa = starRa, starDec = starDec)
        chi2Maps = self.opdfit['chi2Maps']        
        chi2Map = np.zeros([nx, ny])                
        for c in range(self.nchannel):
            for dit in range(self.ndit):
                f = scipy.interpolate.interp1d(opdValues[c, :], chi2Maps[dit, c, :], kind = 'quadratic', assume_sorted = True)
                chi2Map[:, :] = chi2Map[:, :] + f(opds[dit, c, :, :])
        chi2Min = np.min(chi2Map)
        i, j = np.where(chi2Map == chi2Min)
        i, j = (i[0], j[0])
        xBest = xValues[i]
        yBest = yValues[j]
        # recalculate the best RA/DEC fit from single channel/dit fit in terms of OPD
        Yfit, YfitPlanet, wavelet, sqrsum = self.fitPlanetDitByDit(xBest, yBest, error = True, useStarModel = useStarModel, visInst = visInst, simultaneousStarFit = simultaneousStarFit, starOrder = starOrder)
        self.visRefFit = Yfit
        self.visRefFitMean = np.nanmean(Yfit, axis = 0)
        self.visRefFitPlanet = YfitPlanet
        self.visRefFitPlanetMean = np.nanmean(YfitPlanet, axis = 0)        
        self.fit = {"chi2Map": chi2Map, "chi2Min": chi2Min, "xBest": xBest, "yBest": yBest, "bestFit": Yfit}
        print("Best position found: RA = "+str(xBest)+" mas; DEC = "+str(yBest)+" mas.")
        return None

    
    def fitPlanetSpectrumByChannel(self, opdValues, visInst = None, error = True, useStarModel = False, simultaneousStarFit = False, starOrder = 0):
        """
        Fit a spectrum of the planet under the hypothesis that the planet is located at given separation and position angle (theta)
        visInst is the instrumental phase and amplitude.
        """
        wav = self.wav
        ampInst = np.ones([self.ndit, self.nchannel, self.nwav])
        phaseInst = np.zeros([self.ndit, self.nchannel, self.nwav])            
        if not(visInst is None):
            ampInst = ampInst*np.abs(visInst)[None, :, :]
            phaseInst = phaseInst + np.angle(visInst)[None, :, :]            
        if useStarModel == True: 
            wavelet = ampInst*np.exp(-1j*2*np.pi*1e-6*opdValues[:, :, None]/wav[None, None, :])
            for c in range(self.nchannel):
                wavelet[:, c, :] = wavelet[:, c, :] - np.dot(np.dot(wavelet[:, c, :], np.linalg.pinv(self.visStarModel[c, :, :])), self.visStarModel[c, :, :])
        else:
            wavelet = ampInst*np.exp(-1j*2*np.pi*1e-6*opdValues[:, :, None]/wav[None, None, :])            
#            wavelet = np.abs(self.visRefFit - self.visRefFitPlanet)*np.exp(-1j*2*np.pi*1e-6*opdValues[:, :, None]/wav[None, None, :])
        Yfit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        spectrum = np.zeros([self.nchannel, self.nwav])
        spectrum_err = np.zeros([self.nchannel, self.nwav])        
        # ignore all nan points in the fit by setting all them to 0
        visRef = np.copy(self.visRef)#- self.visRefFit + self.visRefFitPlanet)
        ind = np.where(np.isnan(visRef))
        visRef[ind] = 0
        wavelet[ind] = 0
        Y = np.zeros([self.ndit, 2])
        X = np.zeros([self.ndit, 2])
        Xflat = np.zeros([1, self.ndit*2])
        Yflat = np.zeros([1, self.ndit*2])
        for k in range(self.nwav):
            for c in range(self.nchannel):
                if (error == True):
                    Y[:, 0] = np.real(visRef[:, c, k])/np.real(self.visErr[:, c, k])
                    Y[:, 1] = np.imag(visRef[:, c, k])/np.imag(self.visErr[:, c, k])                
                    X[:, 0] = np.real(wavelet[:, c, k])/np.real(self.visErr[:, c, k])
                    X[:, 1] = np.imag(wavelet[:, c, k])/np.imag(self.visErr[:, c, k])
                else:
                    Y[:, 0] = np.real(visRef[:, c, k])
                    Y[:, 1] = np.imag(visRef[:, c, k])
                    X[:, 0] = np.real(wavelet[:, c, k])
                    X[:, 1] = np.imag(wavelet[:, c, k])
                Yflat[0, :] = Y.flatten()
                Xflat[0, :] = X.flatten()
                XflatInv = np.linalg.pinv(Xflat)
                A = np.dot(Yflat, XflatInv)
                Aerr = np.dot(np.dot(XflatInv.T, np.eye(self.ndit*2)), XflatInv) 
                spectrum[c, k] = A[0, 0]
                spectrum_err[c, k] = Aerr[0, 0]**0.5                
        Yfit = wavelet*spectrum[None, :, :]
        sqrsum = np.nansum(np.real(visRef - Yfit)**2/np.real(self.visErr)**2 + np.imag(visRef - Yfit)**2/np.imag(self.visErr)**2)
        self.visRefFitSpec = Yfit
        self.visRefFitSpecMean = np.mean(Yfit, axis = 0)        
        return (Yfit, wavelet, sqrsum, spectrum, spectrum_err)        
    


