# -*- coding: utf-8 -*-
"""
A class to load the data from a Gravity file visOiFits
@author: mnowak
"""
import numpy as np
import scipy.sparse as sp
from scipy.linalg import lapack
import astropy.io.fits as fits
from .genericOiFits import GenericOiFits 
import scipy.interpolate
from .gravityGlobals import *
from .complexstats import *
import itertools

try:
    import patiencebar
    PBAR = True
except:
    PBAR = False

class VisOiFits(GenericOiFits):
    """
    This class is used to gather and manipulate data from the VIS oi in gravity files. 

    Args:
      filename (str): the name of the fits file from which to load the data
      extension (int): extension for the OI, used in calls to astropy.io.fits.getdata. Usually 9 or 10 depending on polarization
      reduction (str): Level of reduction can be 'astrored' or 'scivis'. 
      mode (str): can be 'singlefield' or 'dualfield'. 

    Attributes:
      u, v (float[ndits, nchannels]): UV values of the baselines (in m)
      uCoord, vCoord (float[ndits, nchannels, nwav]): U and V divided by lambda over the third dimension (rad^-1)
      freq ([float[ndits, nchannels, nwav]): (uCoord**2+vCoord**2)**0.5
      visData, visErr (float[ndits, nchannels, nwav]): complex visibilities straight from pipeline (no corrections when zorking with astrored)
      visRef (float[ndits, nchannels, nwav]): complex visibilities corrected for FT zero point
    """
    def __init__(self, filename, extension = None, reduction = None, mode = None, neglect_covar = False):
        # load generic stuff. VisOi has 6 channels (6 baselines)
        super(VisOiFits, self).__init__(filename = filename, oiname = VIS_OI_NAME, nchannel = 6, reduction = reduction, extension = extension, mode = mode)
        self.neglect_covar = neglect_covar # store this info for further calculations
        self.stationIndices = self.fits.field("STA_INDEX")[0:self.nchannel]
        self.u=self.fits.field("UCOORD").reshape(self.ndit, self.nchannel)
        self.v=self.fits.field("VCOORD").reshape(self.ndit, self.nchannel)
        self.uCoord=self.u[:, :, None]/self.wav  # U V divided by wavelength are usually more usefull in calculations
        self.vCoord=self.v[:, :, None]/self.wav
        self.freq = np.abs(self.uCoord + 1j*self.vCoord)
        self.visData= self.loadData("VISDATA")
        self.visErr= self.loadData("VISERR")
        # retrieve flag and clean visData accordingly
        self.flag = self.loadData("FLAG")
        try:
            self.rejflag  = self.fits.field('REJECTION_FLAG').reshape(self.ndit, self.nchannel, -1)
        except:
            self.rejflag = np.zeros([self.ndit, self.nchannel, self.nwav], "int")            
        self.flag = self.flag | ((self.rejflag & 19) >0)            
        self.visData[np.where(self.flag)] = 0
        self.flagCov = np.zeros([self.ndit, self.nchannel, self.nwav, self.nwav], "bool")
        indx = np.where(self.flag)
        self.flagCov[indx] = True # flag the columns
        self.flagCov = np.transpose(self.flagCov, [0, 1, 3, 2]) # transpose to flag rows
        self.flagCov[indx] = True  # flag rows
        self.flagCov = np.transpose(self.flagCov, [0, 1, 3, 2]) # return to original ordering
        # visData and visErr are the raw 'data'. Depending on the exact mode, wa can also calculate
        # a metrology-corrected complex visibiliy visRef
        if ((mode == "dualfield") or (mode == "singlefield")): # the reduction for visOi is actually the same between the two modes
            if reduction == "astrored":
                # V_Factor
                self.vFactor= self.loadData("V_FACTOR")
                # FT data
                self.visDataFt= self.fits.field("VISDATA_FT").reshape(self.ndit, self.nchannel, -1)
                self.nwavFt = len(self.visDataFt[0, 0, :])        
                # phase ref data
                self.phaseRef = self.loadData("PHASE_REF")
                self.phaseRefCoeff=self.fits.field('PHASE_REF_COEFF').reshape(self.ndit, self.nchannel, -1)
                self.opdDisp = self.loadData("OPD_DISP")            
                self.gDelayDisp = self.fits.field("GDELAY_DISP").reshape(self.ndit, self.nchannel)
                self.phaseDisp = self.loadData("PHASE_DISP")
                self.visRef = self.visData*np.exp(1j*self.phaseRef)
                # create covariance and pseudo-covariance matrices from pipeline visErr data                
                self.visRefCov = np.array([[None for k in range(self.nchannel)] for k in range(self.ndit)])
                self.visRefPcov = np.array([[None for k in range(self.nchannel)] for k in range(self.ndit)])
                for dit, c in itertools.product(range(self.ndit), range(self.nchannel)):
                    self.visRef[dit, c, :] = self.visData[dit, c, :]*np.exp(1j*self.phaseRef[dit, c, :])
                    # THESE FORMULA ARE ONLY VALID BECAUSE visCov and visPcov ARE DIAGONAL                                                                            
                    if neglect_covar:
                        # neglect non-diagonal terms
                        self.visRefCov[dit, c] = np.real(self.visErr[dit, c, :])**2+np.imag(self.visErr[dit, c, :])**2
                        self.visRefPcov[dit, c] = (np.real(self.visErr[dit, c, :])**2-np.imag(self.visErr[dit, c, :])**2)*np.exp(2*1j*self.phaseRef[dit, c, :])
                    else:
                        self.visRefCov[dit, c] = sp.csc_matrix(np.diag(np.real(self.visErr[dit, c, :])**2+np.imag(self.visErr[dit, c, :])**2))
                        self.visRefPcov[dit, c] = sp.csc_matrix(np.diag( (np.real(self.visErr[dit, c, :])**2-np.imag(self.visErr[dit, c, :])**2)*np.exp(2*1j*self.phaseRef[dit, c, :]) ))
            elif reduction == "scivis":
                self.visAmp = self.loadData("VISAMP")
                self.visPhi = self.loadData("VISPHI")/180.0*np.pi
                self.visAmpErr = self.loadData("VISAMPERR")
                self.visPhiErr = self.loadData("VISPHIERR")/180.0*np.pi
                self.visRef = np.copy(self.visData) # if scivis, FT phase ref already taken into account by the pipeline
                self.visRefCov = np.array([[None for k in range(self.nchannel)] for k in range(self.ndit)])
                self.visRefPcov = np.array([[None for k in range(self.nchannel)] for k in range(self.ndit)])
                for dit, c in itertools.product(range(self.ndit), range(self.nchannel)):
                    self.visRefCov[dit, c] = sp.csc_matrix(np.diag(np.real(self.visErr[dit, c, :])**2+np.imag(self.visErr[dit, c, :])**2))
                    self.visRefPcov[dit, c] = sp.csc_matrix(np.diag(np.real(self.visErr[dit, c, :])**2-np.imag(self.visErr[dit, c, :])**2))
            else:
                raise Exception("Unknown reduction: "+str(reduction)+"!")
        else:
            raise Exception("Unknown mode: "+str(mode)+"!")
        self.visRefMean = None # DEPRECATED should get rid of this old crap
        self.visErrMean = None # DEPRECATED should get rid of this old crap
        self.visRefFit = None # DEPRECATED should get rid of this old crap
        self.visRefFitSpec = None # DEPRECATED should get rid of this old crap
        return None

    def removeDits(self, indices):
        """
        Remove a number of dits from the data. All dit-based attributes are affected, including covariance matrices.
        The ndit attribute is reduced accordingle.
        indices must be a list
        """
        dits_to_keep = [dit for dit in range(self.ndit) if not(dit in indices)]
        if self.reduction != "astrored":
            raise Exception("Dit filtering is only available for astrored data, not {}".format(self.reduction))
        self.u = self.u[dits_to_keep]
        self.v = self.v[dits_to_keep]
        self.uCoord = self.uCoord[dits_to_keep]
        self.vCoord = self.vCoord[dits_to_keep]
        self.freq = self.freq[dits_to_keep]
        self.visData = self.visData[dits_to_keep]
        self.visErr = self.visErr[dits_to_keep]
        self.flag = self.flag[dits_to_keep]
        self.rejflag = self.rejflag[dits_to_keep]
        self.visDataFt = self.visDataFt[dits_to_keep]
        self.vFactor = self.vFactor[dits_to_keep]
        self.opdDisp = self.opdDisp[dits_to_keep]
        self.phaseDisp = self.phaseDisp[dits_to_keep]                
        self.phaseRefCoeff = self.phaseRefCoeff[dits_to_keep]
        self.gDelayDisp = self.gDelayDisp[dits_to_keep]
        self.visRef = self.visRef[dits_to_keep]
        self.flagCov = self.flagCov[dits_to_keep]
        self.visRefCov = self.visRefCov[dits_to_keep]
        self.visRefPcov = self.visRefPcov[dits_to_keep]
        self.ndit = len(dits_to_keep)
        return None

    def flagPoints(self, indx):
        """ add the given indices as bad points by setting the flag to True, and also update the covariance matrix flags accordingly """
        self.flag[indx] = True
        self.flagCov[indx] = True # flag entire columns in the covariance matrix        
        if not(self.neglect_covar):
            self.flagCov = np.transpose(self.flagCov, [0, 1, 3, 2]) # transpose to flag the rows
        return None

    def calculatePhaseRefArclength(self):
        """ Calculate the arclength for the polynomial fit to phaseRef """
        # calculate normalized wavelength grid used to fit phaseRef
        w0 = self.wav.mean()
        wref = (w0/self.wav - 1)/(np.max(self.wav) - np.min(self.wav))*w0
        # prepare array for values   
        phaseRefArclength = np.zeros([self.ndit, self.nchannel])
        for dit in range(self.ndit):
            for c in range(self.nchannel):
                coeffs = self.phaseRefCoeff[dit, c, :]
                # calculate the coeffs of 1+(dy/dx)**2
                arclengthpolycoeff = np.array([1+coeffs[1]**2, 4*coeffs[1]*coeffs[2], 4*coeffs[2]**2+6*coeffs[1]*coeffs[3], 12*coeffs[2]*coeffs[3], 9*coeffs[3]**2])
                # integrate sqrt(1+(dx/dy)**2) do get arclength. Minus sign because wref is decreasing
                phaseRefArclength[dit, c] = -np.trapz(np.sqrt(np.polyval(arclengthpolycoeff[::-1], wref)), wref)
        return phaseRefArclength
    
    def scaleVisibilities(self, factor):
        """Multiply visData, visRef, visAmp, and associated errors by a scale factor. If scale factor is an array, assume it's 1 value for each dit and baseline"""
        if type(factor) is float:
            factor = np.tile(factor, [self.ndit, self.nchannel])
        for dit in range(self.ndit):
            for c in range(self.nchannel):
                # visData
                self.visData[dit, c, :] = self.visData[dit, c, :]*factor[dit, c]
                # visRef
                self.visRef[dit, c, :] = self.visRef[dit, c, :]*factor[dit, c]
                self.visRefCov[dit, c] = self.visRefCov[dit, c].multiply(factor[dit, c]**2)
                self.visRefPcov[dit, c] = self.visRefPcov[dit, c].multiply(factor[dit, c]**2)
                # visFt
                if self.reduction == "astrored":
                    self.visDataFt[dit, c, :] = self.visDataFt[dit, c, :]*factor[dit, c]
                # visAmp
                if self.reduction == "scivis":
                    self.visAmp[dit, c, :] = self.visAmp[dit, c, ]*factor[dit, c]
                    self.visAmpErr[dit, c, ] = self.visAmpErr[dit, c, :]*factor[dit, c]
        return None

    def scaleVisRef(self, factor):
        """Scale only the visRef array and its error bars by the given factor, which must be an array of size (ndits, nchannels, nwav)"""
        for dit, c in itertools.product(range(self.ndit), range(self.nchannel)):
            self.visRef[dit, c, :] = factor[dit, c, :]*self.visRef[dit, c, :]
            self.visRefCov[dit, c] = self.visRefCov[dit, c].multiply(factor[dit, c]**2)
            self.visRefPcov[dit, c] = self.visRefPcov[dit, c].multiply(factor[dit, c]**2)
        return None
    
    def getOpd(self, ra, dec):
        """
        return the list of OPDs calculated for a source at given ra/dec (in mas) for each u v coordinate of each channel and each dit"""
        ra_rad = ra/1000.0/3600.0/360.0*2*np.pi
        dec_rad = dec/1000.0/3600.0/360.0*2*np.pi        
        return (self.u*ra_rad + self.v*dec_rad)*1e6

    def getWavelet(self, ra, dec, visInst = None, contrast = None):
        """ Return a model of a point source at given ra/dec multiplied by contrast*visInst if given"""
        wavelet = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        if visInst is None:
            visInst = np.ones([self.nchannel, self.nwav])
        if contrast is None:
            contrast = np.ones(self.nwav)
        opd = self.getOpd(ra, dec)
        for dit in range(self.ndit):
            for c in range(self.nchannel):
                wavelet[dit, c, :] = contrast[:]*visInst[c, :]*np.exp(-1j*2*np.pi/(self.wav*1e6)*opd[dit, c])
        return wavelet

    def computeMean(self):
        """
        Calculate and store the mean of each attribute dataset
        """
        self.u = np.tile(np.mean(self.u, axis = 0), [1, 1])
        self.v = np.tile(np.mean(self.v, axis = 0), [1, 1])
        noflagsum = np.sum(~self.flag, axis = 0)
        noflagsum[np.where(noflagsum == 0)] = 1
        self.visData = np.tile(np.sum(self.visData*~self.flag, axis = 0)/noflagsum, [1, 1, 1])
        self.visRef = np.tile(np.sum(self.visRef*~self.flag, axis = 0)/noflagsum, [1, 1, 1])
        self.uCoord = np.tile(np.sum(self.uCoord*~self.flag, axis = 0)/noflagsum, [1, 1, 1])
        self.vCoord = np.tile(np.sum(self.vCoord*~self.flag, axis = 0)/noflagsum, [1, 1, 1])
        for c in range(self.nchannel):
            self.visRefCov[0, c] = np.sum(self.visRefCov[:, c]) # for each channel we sum over dits
            self.visRefPcov[0, c] = np.sum(self.visRefPcov[:, c])
        self.visRefCov = self.visRefCov[0:1, :]/self.ndit**2 # keep only the first one containing average, and divide to get errors on mean
        self.visRefPcov = self.visRefPcov[0:1, :]/self.ndit**2
        self.flag = np.tile(np.all(self.flag, axis = 0), [1, 1, 1])
        self.phaseRefCoeff = np.tile(np.mean(self.phaseRefCoeff, axis = 0), [1, 1, 1])                
        self.ndit = 1
        return None

    def addPhase(self, phase):
        """
        Add the given phase (given as float[ndit, nchannels, nwav] for each baseline and wavelength) to the current visRef. Also update the error accordingly.
        """
        # user friendly. If size of phase is [nchannel, nwav], then add the same to each dit
        if len(phase.shape) == 2:
            phase = np.tile(phase, [self.ndit, 1, 1])
        for dit, c in itertools.product(range(self.ndit), range(self.nchannel)):
            phi = np.exp(1j*phase[dit, c, :])
            self.visRef[dit, c, :] = phi*self.visRef[dit, c, :]
            if self.neglect_covar:
                self.visRefCov[dit, c] = self.visRefCov[dit, c] * np.abs(phi)**2
                self.visRefPcov[dit, c] = self.visRefPcov[dit, c] * phi**2               
            else:
                self.visRefCov[dit, c] = np.dot(sp.csc_matrix(np.diag(phi)) , np.dot(self.visRefCov[dit, c], sp.csr_matrix(np.diag(adj(phi))) ) )
                self.visRefPcov[dit, c] = np.dot(sp.csc_matrix(np.diag(phi)) , np.dot(self.visRefPcov[dit, c], sp.csr_matrix(np.diag(phi)) ) )
        if not(self.visRefMean is None):
            self.visRefMean = np.mean(self.visRef, axis = 0)
        if not(self.visRefFit is None):
            self.visRefFit = self.visRefFit * np.exp(1j*phase)
        if not(self.visRefFitSpec is None):
            self.visRefFitSpec = self.visRefFitSpec * np.exp(1j*phase)
        return None

    def recenterPhase(self, xvalue, yvalue, radec = True):
        """
        Recenter visRef and visRefError on to the given position by shifting the phases
        
        Args:
          xvalue, yvalue (float): the new coordinates (referenced to the current position) at which to shift the visibilities
          radec (bool): if True (default), x/y are interpreted as ra/dec in mas. If False, x is separation (mas), and y is position angle (in deg)
        """
        if (radec == False):
            theta_rad = xvalue/360.0*2*np.pi
            this_u = (np.sin(theta_rad)*self.uCoord + np.cos(theta_rad)*self.vCoord)/1e7            
            phase = 2*np.pi*this_u*1e7*separation/3600.0/360.0/1000.0*2*np.pi
        else:
            this_u = (xvalue*self.uCoord + yvalue*self.vCoord)/1e7
            phase = 2*np.pi*this_u*1e7/3600.0/360.0/1000.0*2*np.pi
        self.addPhase(phase)
        return None
    
    def fitStarDitByDit(self, visStar, order = 2, ra = 0, dec = 0, saturation = None):
        """
        Fit a star model to the visRef. The star model is the given visStar multiplied by a polynomial in
        wavelength of the given order.
        """
        # create the model for the star
        ampStar = np.abs(visStar)
        phaseStar = np.angle(visStar)
        X = np.zeros([self.nchannel, (order+1), self.nwav], 'complex')
#        this_u = np.mean((ra*self.uCoord + dec*self.vCoord)/1e7, axis = 0)
#        phaseFactor = np.exp(-1j*2*np.pi*this_u*1e7/3600.0/360.0/1000.0*2*np.pi)
        for c in range(self.nchannel):
            for k in range(order+1):
                X[c, k, :] = ampStar[c, :]*(1e7*(self.wav - np.mean(self.wav)))**k
#                X[c, k, :] = ampStar[c, :]*(1.9e-6/self.wav)**k
#            for k in range(order+1):
#                X[c, order+1+k, :] = ampStar[c, :]*(1e7*(self.wav - np.mean(self.wav)))**k * phaseFactor[c, :]
#                X[c, order+1+k, :] = 1*(1.9e-6/self.wav)**k*0
#                if (saturation == "squared"):
#                    X[c, order+1+k, :] = (ampStar[c, :]**2)*(1.9e-6/self.wav)**k
#                if (saturation == "constant"):
#                    X[c, order+1+k, :] = 1#*(1e7*(self.wav - np.mean(self.wav)))**k
#                else:
#                    X[c, order+1+k, :] = 0
        # store the model as is
        self.visStarModel = X
        # prepare the fit
        Yfit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        coeffs = np.zeros([self.ndit, self.nchannel, (order+1)], 'complex')
        # star to fit, dit by dit, channel by channel
        for dit in range(self.ndit):    
            for c in range(self.nchannel):
                Y = np.zeros([self.nwav], 'complex')
                Y[:] = self.visRef[dit, c, :]
                # ignore all nan points in the fit by setting all them to 0
                X = np.copy(self.visStarModel[c, :, :])
                ind = np.where(np.isnan(Y))
                Y[ind] = 0
                for k in range(order+1):
                    X[k, ind] = 0
                A = np.dot(Y, np.linalg.pinv(X))
                coeffs[dit, c, :] = A
                Yfit[dit, c, :] = np.dot(A, X)
                Yfit[dit, c, ind] = float('nan') + float('nan')*1j
        self.coeffs = coeffs
        # store everything for plotting later on
        self.visStar = Yfit
        self.visStarMean = np.nanmean(Yfit, axis = 0)
        Yres = self.visRef - Yfit
#        self.visRef = Yres    
#        self.visRefMean = np.nanmean(Yres, axis = 0)        
        return None
                             
    def fitOpdDitByDit(self, opdValue, channel = None, visInst = None, error = True, spectrum = None, useStarModel = False, simultaneousStarFit = False, starOrder = 1, starRa = 0, starDec = 0):
        """
        Fit each dit using a model made of a given spectrum multiplied by the visInst amplitude
        and a wavelet corresponding to given opd
        OPD given in micrometers
        It is also possible to simultaneously fit the star model with the opd by putting the simultaneousStarFit to True
        """
        # useStarModel and simultaneousStarFit cannot be both true
        if ((simultaneousStarFit==True) and (useStarModel == True)):
            raise Exception("simultaneousStarFit and useStarModel keyword arguments cannot be both set to 'True'")
        if (channel is None):
            raise Exception("Channel keyword argument cannot be 'None'!")
        wav = self.wav
        ampInst = np.ones([self.ndit, self.nwav])
        phaseInst = np.ones([self.ndit, self.nwav])
        # if visInst is given, use it as a base shape for the amplitude and phase
        if not(visInst is None):
            ampInst = ampInst*np.abs(visInst)[None, :]
            phaseInst = phaseInst*np.angle(visInst)[None, :]
        # if spectrum is given, also use it to modulate the expected amplitude, but else, set it to all ones
        if spectrum is None:
            spectrum = np.ones(self.nwav)
        wavelet = spectrum[None, :]*ampInst*np.exp(-1j*2*np.pi/(wav*1e6)*opdValue)
        # if user requested to use a pre-calculated starModel, check is this model is present, and if yes, project
        # the wavelet model onto the axis orthogonal to this starModel
        if useStarModel == True:
            wavelet = wavelet - np.dot(np.dot(wavelet, np.linalg.pinv(self.visStarModel[channel, :, :])), self.visStarModel[channel, :, :])
        Yfit = np.zeros([self.ndit, self.nwav], 'complex')
        YfitPlanet = np.zeros([self.ndit, self.nwav], 'complex')        
        # ignore all nan points in the fit by setting all them to 0
        visRef = np.copy(self.visRef[:, channel, :])
        ind = np.where(np.isnan(visRef))
        visRef[ind] = 0
        wavelet[ind] = 0
        sqrsums = np.zeros([self.ndit])
        for dit in range(self.ndit):
            # prepare the matrices for linear fit Y=AX
            Y = np.zeros([1, 2*self.nwav])            
            if (simultaneousStarFit == True):
                X = np.zeros([2*(starOrder+1)+1, 2*self.nwav])
            else:
                X = np.zeros([2, 2*self.nwav])                                        
            if (error == True):
                Y[0, 0:self.nwav] = np.real(visRef[dit, :])/np.real(self.visErr[dit, channel, :])
                Y[0, self.nwav:2*self.nwav] = np.imag(visRef[dit, :])/np.imag(self.visErr[dit, channel, :])
                if (simultaneousStarFit == True): 
                    X[0, 0:self.nwav] = np.real(wavelet[dit, :])/np.real(self.visErr[dit, channel, :])
                    X[0, self.nwav:2*self.nwav] = np.imag(wavelet[dit, :])/np.imag(self.visErr[dit, channel, :])
                    for k in range(starOrder+1):
                        X[2*k+1, 0:self.nwav] = ampInst[dit, :]*(1e7*(self.wav - np.mean(self.wav)))**k/np.real(self.visErr[dit, channel, :])
                        X[2*k+1, self.nwav:2*self.nwav] = 0
                        X[2*k+2, 0:self.nwav] = 0
                        X[2*k+2, self.nwav:2*self.nwav] = ampInst[dit, :]*(1e7*(self.wav - np.mean(self.wav)))**k/np.imag(self.visErr[dit, channel, :])
                else:
                    X[0, 0:self.nwav] = np.real(wavelet[dit, :])/np.real(self.visErr[dit, channel, :])
                    X[0, self.nwav:2*self.nwav] = np.imag(wavelet[dit, :])/np.imag(self.visErr[dit, channel, :])
#                    print("Warning in visOiFits line 245!!!")
                    X[1, 0:self.nwav] = np.real(wavelet[dit, :])/np.real(self.visErr[dit, channel, :]) * (1e7*(self.wav - np.mean(self.wav)))*0
                    X[1, self.nwav:2*self.nwav] = np.imag(wavelet[dit, :])/np.imag(self.visErr[dit, channel, :]) * (1e7*(self.wav - np.mean(self.wav)))*0
            else:
                Y[0, 0:self.nwav] = np.real(visRef[dit, :])
                Y[0, self.nwav:2*self.nwav] = np.imag(visRef[dit, :])
                if (simultaneousStarFit == True): 
                    X[0, 0:self.nwav] = np.real(wavelet[dit, :])
                    X[0, self.nwav:2*self.nwav] = np.imag(wavelet[dit, :])
                    for k in range(starOrder+1):
                        X[2*k+1, 0:self.nwav] = ampInst[dit, :]*(1e7*(self.wav - np.mean(self.wav)))**k
                        X[2*k+1, self.nwav:2*self.nwav] = 0
                        X[2*k+2, 0:self.nwav] = 0
                        X[2*k+2, self.nwav:2*self.nwav] = ampInst[dit, :]*(1e7*(self.wav - np.mean(self.wav)))**k
                else:
                    X[0, 0:self.nwav] = np.real(wavelet[dit, :])
                    X[0, self.nwav:2*self.nwav] = np.imag(wavelet[dit, :])
            A = np.dot(Y, np.linalg.pinv(X))                    
            if A[0, 0] < 0:
                A[0, 0] = 0
                X[0, :] = 0 
                A = np.dot(Y, np.linalg.pinv(X))                
            result = np.dot(A, X)
            if (error == True):
                Yfit[dit, :] = result[0, 0:self.nwav]*np.real(self.visErr[dit, channel, :]) + 1j*result[0, self.nwav:2*self.nwav]*np.imag(self.visErr[dit, channel, :])
                YfitPlanet[dit, :] = A[0, 0]*X[0, 0:self.nwav]*np.real(self.visErr[dit, channel, :]) + 1j*A[0, 0]*X[0, self.nwav:2*self.nwav]*np.imag(self.visErr[dit, channel, :])
            else:
                Yfit[dit, :] = result[0, 0:self.nwav] + 1j*result[0, self.nwav:2*self.nwav]
                YfitPlanet[dit, :] = A[0, 0]*X[0, 0:self.nwav] + 1j*A[0, 0]*X[0, self.nwav:2*self.nwav]                                    
            Yres = self.visRef[dit, channel, :] - Yfit[dit, :]
            if (error == True):
                sqrsums[dit] = np.nansum(np.real(Yres)**2/np.real(self.visErr[dit, channel, :])**2 + np.imag(Yres)**2/np.imag(self.visErr[dit, channel, :])**2)
            else:
                sqrsums[dit] = np.nansum(np.real(Yres)**2 + np.imag(Yres)**2)
        return (Yfit, YfitPlanet, wavelet, sqrsums)

    
    def computeOpdChi2Maps(self, opdValues, visInst = None, useStarModel = False, simultaneousStarFit = False, starOrder = 0, starRa = 0, starDec = 0):
        nvalues = np.shape(opdValues)[1]
        chi2Maps = np.zeros([self.ndit, self.nchannel, nvalues]) # will contain the 1D chi-square for map for each channel and each dit
        chi2Mins = np.zeros([self.ndit, self.nchannel])+np.inf # will contain the best chi2
        opdBests = np.zeros([self.ndit, self.nchannel])+np.inf # will contain the opd corresponding to the best chi2
        bestFit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex') # will be filled with the best fits for each channel and each dit
        bestWavelet = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        if PBAR:
            pbar = patiencebar.Patiencebar(valmax = nvalues, title = "Calculating the chi-square map...")
        for k in range(nvalues):
            # notify the user of progress
            if PBAR:
                pbar.update()
            else:
                print(str(k+1)+"/"+str(nvalues))
            for c in range(self.nchannel):
                # if visInst is given, extract the correct channel
                if not(visInst is None):
                    thisVisInst = visInst[c, :]
                else:
                    thisVisInst = None
                Yfit, YfitPlanet, wavelet, sqrsums = self.fitOpdDitByDit(opdValues[c, k], channel = c, visInst = thisVisInst, error = True, useStarModel = useStarModel, simultaneousStarFit = simultaneousStarFit, starOrder = starOrder, starRa = starRa, starDec = starDec)
                chi2Maps[:, c, k] = sqrsums
                for dit in range(self.ndit):
                    if sqrsums[dit] < chi2Mins[dit, c]:
                        chi2Mins[dit, c] = sqrsums[dit]
                        opdBests[dit, c] = opdValues[c, k]
                        bestFit[dit, c, :] = Yfit[dit, :]
                        bestWavelet[dit, c, :] = wavelet[dit, :]
        self.opdfit = {"chi2Maps": chi2Maps, "chi2Mins": chi2Mins, "opdValues":opdValues, "bestFit": bestFit, "opdBests": opdBests}
        self.visRefOpdFit = bestFit
        self.visRefOpdFitMean = np.nanmean(bestFit, axis = 0)
        self.opdWavelet = bestWavelet        
        return None

    def fitPlanetDitByDit(self, xvalue, yvalue, error = True, visInst = None, useStarModel = False, simultaneousStarFit = False, starOrder = 0):
        Yfit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        YfitPlanet = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')        
        wavelet = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        sqrsum = np.inf
        for c in range(self.nchannel):
            for dit in range(self.ndit):
                opd = self.getOpd(xvalue, yvalue)[dit, c]
                if not(visInst is None):
                    thisVisInst = visInst[c, :]                    
                else:
                    thisVisInst = None
                (channelYfit, channelYfitPlanet, channelWavelet, channelSqrsums) = self.fitOpdDitByDit(opd, channel = c, visInst = thisVisInst, useStarModel = useStarModel, error = error, simultaneousStarFit = simultaneousStarFit, starOrder = starOrder)
                Yfit[dit, c, :] = channelYfit[dit, :]
                YfitPlanet[dit, c, :] = channelYfitPlanet[dit, :]                
                wavelet[dit, c, :] = channelWavelet[dit, :]
                sqrsum = sqrsum + np.nansum(channelSqrsums)
        return Yfit, YfitPlanet, wavelet, sqrsum

    def computeChi2Map(self, xValues, yValues, nopds = None, visInst = None, recomputeOpdChi2Maps = True, useStarModel = False, simultaneousStarFit = False, starOrder = 0, starRa = 0, starDec = 0):
        """
        Use the opdChi2Map calculation to create a RA/DEC chi2 map, and extract the best fit in terms of RA/DEC from it
        RA and DEC values must be given in mas
        """
        nx = len(xValues)
        ny = len(yValues)
        if nopds is None:
            nopds = np.max([nx, ny])
        opds = np.zeros([self.ndit, self.nchannel, nx, ny])
        for i in range(nx):
            for j in range(ny):
                opds[:, :, i, j] = self.getOpd(xValues[i], yValues[j])
        if recomputeOpdChi2Maps:
            opdValues = np.zeros([self.nchannel, nopds])
            for c in range(self.nchannel):
                opdValues[c, :] = np.linspace(np.min(opds[:, c, :, :])-1, np.max(opds[:, c, :, :])+1, nopds)
            self.computeOpdChi2Maps(opdValues, visInst = visInst, useStarModel = useStarModel, simultaneousStarFit = simultaneousStarFit, starOrder = starOrder, starRa = starRa, starDec = starDec)
        chi2Maps = self.opdfit['chi2Maps']        
        chi2Map = np.zeros([nx, ny])                
        for c in range(self.nchannel):
            for dit in range(self.ndit):
                f = scipy.interpolate.interp1d(opdValues[c, :], chi2Maps[dit, c, :], kind = 'quadratic', assume_sorted = True)
                chi2Map[:, :] = chi2Map[:, :] + f(opds[dit, c, :, :])
        chi2Min = np.min(chi2Map)
        i, j = np.where(chi2Map == chi2Min)
        i, j = (i[0], j[0])
        xBest = xValues[i]
        yBest = yValues[j]
        # recalculate the best RA/DEC fit from single channel/dit fit in terms of OPD
        Yfit, YfitPlanet, wavelet, sqrsum = self.fitPlanetDitByDit(xBest, yBest, error = True, useStarModel = useStarModel, visInst = visInst, simultaneousStarFit = simultaneousStarFit, starOrder = starOrder)
        self.visRefFit = Yfit
        self.visRefFitMean = np.nanmean(Yfit, axis = 0)
        self.visRefFitPlanet = YfitPlanet
        self.visRefFitPlanetMean = np.nanmean(YfitPlanet, axis = 0)        
        self.fit = {"chi2Map": chi2Map, "chi2Min": chi2Min, "xBest": xBest, "yBest": yBest, "bestFit": Yfit}
        print("Best position found: RA = "+str(xBest)+" mas; DEC = "+str(yBest)+" mas.")
        return None


    
    def fitPlanetSpectrumByChannel(self, opdValues, visInst = None, error = True, useStarModel = False, simultaneousStarFit = False, starOrder = 0):
        """
        Fit a spectrum of the planet under the hypothesis that the planet is located at given separation and position angle (theta)
        visInst is the instrumental phase and amplitude.
        """
        wav = self.wav
        ampInst = np.ones([self.ndit, self.nchannel, self.nwav])
        phaseInst = np.zeros([self.ndit, self.nchannel, self.nwav])            
        if not(visInst is None):
            ampInst = ampInst*np.abs(visInst)[None, :, :]
            phaseInst = phaseInst + np.angle(visInst)[None, :, :]            
        if useStarModel == True: 
            wavelet = ampInst*np.exp(-1j*2*np.pi*1e-6*opdValues[:, :, None]/wav[None, None, :])
            for c in range(self.nchannel):
                wavelet[:, c, :] = wavelet[:, c, :] - np.dot(np.dot(wavelet[:, c, :], np.linalg.pinv(self.visStarModel[c, :, :])), self.visStarModel[c, :, :])
        else:
            wavelet = ampInst*np.exp(-1j*2*np.pi*1e-6*opdValues[:, :, None]/wav[None, None, :])            
#            wavelet = np.abs(self.visRefFit - self.visRefFitPlanet)*np.exp(-1j*2*np.pi*1e-6*opdValues[:, :, None]/wav[None, None, :])
        Yfit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        spectrum = np.zeros([self.nchannel, self.nwav])
        spectrum_err = np.zeros([self.nchannel, self.nwav])        
        # ignore all nan points in the fit by setting all them to 0
        visRef = np.copy(self.visRef)#- self.visRefFit + self.visRefFitPlanet)
        ind = np.where(np.isnan(visRef))
        visRef[ind] = 0
        wavelet[ind] = 0
        Y = np.zeros([self.ndit, 2])
        X = np.zeros([self.ndit, 2])
        Xflat = np.zeros([1, self.ndit*2])
        Yflat = np.zeros([1, self.ndit*2])
        for k in range(self.nwav):
            for c in range(self.nchannel):
                if (error == True):
                    Y[:, 0] = np.real(visRef[:, c, k])/np.real(self.visErr[:, c, k])
                    Y[:, 1] = np.imag(visRef[:, c, k])/np.imag(self.visErr[:, c, k])                
                    X[:, 0] = np.real(wavelet[:, c, k])/np.real(self.visErr[:, c, k])
                    X[:, 1] = np.imag(wavelet[:, c, k])/np.imag(self.visErr[:, c, k])
                else:
                    Y[:, 0] = np.real(visRef[:, c, k])
                    Y[:, 1] = np.imag(visRef[:, c, k])
                    X[:, 0] = np.real(wavelet[:, c, k])
                    X[:, 1] = np.imag(wavelet[:, c, k])
                Yflat[0, :] = Y.flatten()
                Xflat[0, :] = X.flatten()
                XflatInv = np.linalg.pinv(Xflat)
                A = np.dot(Yflat, XflatInv)
                Aerr = np.dot(np.dot(XflatInv.T, np.eye(self.ndit*2)), XflatInv) 
                spectrum[c, k] = A[0, 0]
                spectrum_err[c, k] = Aerr[0, 0]**0.5                
        Yfit = wavelet*spectrum[None, :, :]
        sqrsum = np.nansum(np.real(visRef - Yfit)**2/np.real(self.visErr)**2 + np.imag(visRef - Yfit)**2/np.imag(self.visErr)**2)
        self.visRefFitSpec = Yfit
        self.visRefFitSpecMean = np.mean(Yfit, axis = 0)        
        return (Yfit, wavelet, sqrsum, spectrum, spectrum_err)        
    
    def phaseModel(self, ra, dec, ratio):
        ra_rad = ra/1000.0/3600.0/360.0*2*np.pi
        dec_rad = dec/1000.0/3600.0/360.0*2*np.pi
        phase = np.zeros([self.ndit, self.nchannel, self.nwav])
        for dit in range(self.ndit):
            for c in range(self.nchannel):
                c1 = np.cos(2*np.pi*(ra_rad*self.uCoord[dit, c, :]+dec_rad*self.vCoord[dit, c, :]))
                s1 = np.sin(2*np.pi*(ra_rad*self.uCoord[dit, c, :]+dec_rad*self.vCoord[dit, c, :]))
                phase[dit, c, :] = np.arctan2(ratio*s1, (1+ratio*c1))*180.0/np.pi
        return phase

    def fitVisRefOpd(self, opd_values, target_spectrum = None, poly_spectrum = None, poly_phase = None, no_inv=False, poly_order = 0, dits = None):
        """ 
        Fit the visibilities in opd space using a point source with a background polynomial model, and taking into account complex errors
        args:
          opd_values (real array): an array of size (nchannels, nopds) giving the opds on which the maps should be calculated
          target_spectrum (opt, real array): array of size (nchannels, nwav) the spectrum of the target, can be different on each baseline. Default is all 1s
          poly_spectrum (opt, real array): array of size (nchannels, nwav) giving the spectrum of the polynomial, can be different on each baseline. Default all 1s.
          poly_order (opt, int): the order of the complex-coeff polynomial by which poly_spectrum is multiplied in the model. Default is 0 (a constant term).
          no_inv (opt, bool): True to avoid explicit inversion of the matrices (can sometimes be faster)
          dits (int list): if you want to restrict the fit to certain dits, you can give the list of dit numbers
        returns:
          opdFit: a dictionnary which contains:
                   "map" an array of size (ndits, nchannels, nopds) giving the full min chi2 maps
                   "best" an array of size (ndit, nchannel) giving the opd of best fit for each dit and baseline
                   "fit" an array of size (ndit, nchannels, nwav) giving the best fit
        """
        # calculate the number of opd values
        nopd = np.shape(opd_values)[1]        
        # if spectra are None, fill with 1s
        if target_spectrum is None:
            target_spectrum = np.ones([self.nchannel, self.nwav])
        if poly_spectrum is None:
            poly_spectrum = np.ones([self.nchannel, self.nwav])
        if dits is None:
            dits = range(self.ndit)
        # prepare the array for opdMaps
        opdChi2Map = np.zeros([self.ndit, self.nchannel, nopd]) + np.inf
        # prepare array to store parameters obtained at each step
        opdParameterMap = np.zeros([self.ndit, self.nchannel, nopd, 2*(poly_order+1)+1])
        # prepare the array for best fit values
        bestOpdFit = np.zeros([self.ndit, self.nchannel, self.nwav], 'complex')
        bestOpdValues = np.zeros([self.ndit, self.nchannel])
        # dit by dit, channel by channel
        for dit in dits:
            for c in range(self.nchannel):
                # filter data points based on flag status
                bad_indices = np.where(self.flag[dit, c, :])
                # if there are not enough valid points to invert the problem, we put chi2 at nan and skip step
                if (self.nwav - len(bad_indices[0])) < (2*(poly_order+1)+2):
                    opdChi2Map[dit, c, :] = float("nan")
                    continue
                visRefNoBad = np.copy(self.visRef[dit, c, :])
                visRefNoBad[bad_indices] = 0
                # retrieve cov and pcov and build to complex_extended covar
                W = self.visRefCov[dit, c].todense()
                Z = self.visRefPcov[dit, c].todense()
                W2 = extended_covariance(W, Z).real
                # the next objective is to calculate W2inv * A2
                # we can either invert W2 completely and the multiply or
                # avoid inversion and calculate the product little by little. The inversion is numpy stuff and is
                # quite slow. However it occurs only once for all dits, and thus for high number of dits it's
                # usually faster. For small number of dits, though, it's slower. And it also takes memory
                # strategy is tied to no_inv kwarg
                if no_inv: # we avoid the inversion and will calculate the product later on
                    W2sp = scipy.sparse.csr_matrix(W2)
                    W2invA2 = np.zeros([2*self.nwav, 2*(poly_order+1)+1], 'complex')
                else: # we invert the W2 matrix explicitly.
                    ZZ, _ = lapack.dpotrf(W2)
                    T, info = lapack.dpotri(ZZ)
                    W2inv = np.triu(T) + np.triu(T, k=1).T
                # now we need to construct A2
                bestChi2 = np.inf # best value found for the chi2 for this dit/channel. used to keep best fit in memory                
                for j in range(nopd):
                    opd = opd_values[c, j]
                    # calculate A matrix
                    A = np.zeros([self.nwav, (poly_order+1)*2+1], 'complex')
                    for p in range(poly_order+1):
                        A[:, 2*p] = poly_spectrum[c, :]*((self.wav-np.mean(self.wav))*1e6)**p
                        A[:, 2*p+1] = 1j*poly_spectrum[c, :]*((self.wav-np.mean(self.wav))*1e6)**p                    
                    # last column requires phase and spectrum
                    phase = 2*np.pi*opd/(self.wav*1e6)
                    A[:, -1] = np.exp(-1j*phase)*target_spectrum[c, :]
                    # filter nan points in the model
                    A[bad_indices, :] = 0
                    # maximum likelihood solution
                    A2 = conj_extended(A)
                    if no_inv:
                        # in the no_inv strategy we don't have W2inv. We solve the linear problem bit by bit
                        for p in range(2*(poly_order+1)+1):
                            W2invA2[:, p] = scipy.sparse.linalg.spsolve(W2sp, A2[:, p])
                    else: # otherwize we use the invert. Faster because computed once and for all
                        W2invA2 = np.dot(W2inv, A2)
                    left = np.real( np.dot(adj(conj_extended(visRefNoBad)), W2invA2) )
                    right = np.real( np.dot(adj(A2), W2invA2) )
                    dzeta = np.transpose(np.dot(left, np.linalg.pinv(right, rcond = 1e-11)))
                    # the target cannot have negative flux. If negative flux redo fit with only polynomial                    
                    if (dzeta[-1] < 0):
                        dzeta[-1] = 0
                        A[:, -1] = 0
                        A2 = conj_extended(A)
                        if no_inv:
                            W2invA2[:, -1] = scipy.sparse.linalg.spsolve(W2sp, A2[:, -1])
                        else:
                            W2invA2 = np.dot(W2inv, A2)
                        left = np.real( np.dot(adj(conj_extended(visRefNoBad)), W2invA2) )
                        right = np.real( np.dot(adj(A2), W2invA2) )
                        dzeta = np.transpose(np.dot(left, np.linalg.pinv(right)))
                        dzeta[-1] = 0
                    # calculate the corresponding chi2
                    vec = conj_extended(visRefNoBad) - np.dot(A2, dzeta)
                    if no_inv:
                        rightVec = scipy.sparse.linalg.spsolve(W2sp, vec)
                        opdChi2Map[dit, c, j] = np.real(np.dot(adj(vec), rightVec))
                        opdParameterMap[dit, c, j, :] = dzeta                        
                    else:
                        opdChi2Map[dit, c, j] = np.real(np.dot(np.dot(adj(vec), W2inv), vec))
                        opdParameterMap[dit, c, j, :] = dzeta
                    # look if this is the best fit
                    if opdChi2Map[dit, c, j] < bestChi2:
                        bestChi2 = opdChi2Map[dit, c, j]
                        bestOpdValues[dit, c] = opd
                        bestOpdFit[dit, c, :] = np.dot(A2, dzeta)[0:self.nwav]
        # create the dict to return
        opdFit = {"map": opdChi2Map, "fit": bestOpdFit, "best": bestOpdValues, "params": opdParameterMap, "grid": opd_values}
        # also keep it as an attribute
        self.opdFit = opdFit
        return opdFit

    def fitVisRefAstrometry(self, ra_values, dec_values, nopd = 100, target_spectrum = None, poly_spectrum = None, no_inv=False, poly_order = 0, return_opd_fit = False, ignore_baselines = None):
        """ 
        Fit the visibilities in opd space (see fitVisRefOpd) and then construct the chi2maps in astrometric coordinates by interpolating
        the maps in opd.
        args:
          ra_values (real array): an array giving the values in RA (mas) for the map
          dec_values (real array): an array giving the values in DEC (mas) for the map
          n_opd: the number of opd values to use when building the opd maps (default is 0)
          target_spectrum: see fitVisRefOpd
          poly_spectrum: see fitVisRefOpd
          poly_order: see fitVisRefOpd
          no_inv: see fitVisRefOpd
          return_opd_fit: also returns the underlying OPD fit (default is false)
          ignore_baselines: an optional list of baseline numbers which should be ignored for the construction of the final chi2Map
        returns:
          astrometryFit: a dictionnary which contains:
                         "map" an array of size (nra, ndec) giving the full min chi2 maps
                         "best" the best astrometric solution on the map (ra, dec)
                         "fit" an array of size (ndit, nchannels, nwav) giving the best fit recalculated for the correct OPD values
                         "opdFit": a copy of the result of the opd fit which was generated during the process
        """
        # get number of points in ra and dec
        nra = len(ra_values)
        ndec = len(dec_values)
        # get list of baselines to be fitted:
        if ignore_baselines is None:
            ignore_baselines = []
        baselines_tofit = [c for c in range(self.nchannel) if not(c in ignore_baselines)]
        # the first task is to generate the opd arrays which are going to be used to generate the opd chi2 maps
        # for this we need to calculate the min and max opd on each baseline for the requested ra and dec ranges
        opdLimits = np.zeros([self.nchannel, 2]) # min and max for each channel
        opdLimits[:, 0] = +np.inf
        opdLimits[:, 1] = -np.inf
        # we'll go tgrough all the values and find the largets and smallest opd
        for ra in ra_values:
            for dec in dec_values:
                opd = self.getOpd(ra, dec) # opd[dit, c] is opd on channel c for dit dit corresponding to the ra/dec
                for c in range(self.nchannel):
                    if np.min(opd[:, c]) < opdLimits[c, 0]: # if bigger than current biggest we keep it
                        opdLimits[c, 0] = np.min(opd[:, c])
                    if np.max(opd[:, c]) > opdLimits[c, 1]: # if smaller than current smallest we keep it
                        opdLimits[c, 1] = np.max(opd[:, c])
        # now we can create the array using request nopd
        opdRanges = np.zeros([self.nchannel, nopd])
        for c in range(self.nchannel):
            opdRanges[c, :] = np.linspace(opdLimits[c, 0], opdLimits[c, 1], nopd)
        # calculate the opd map
        opdFit = self.fitVisRefOpd(opdRanges, target_spectrum = target_spectrum, poly_spectrum = poly_spectrum, poly_order = poly_order, no_inv = no_inv)
        # calculate the chi2Maps from the OPD maps
        chi2Map = np.zeros([self.ndit, nra, ndec])
        bestOpds = np.zeros([self.ndit, self.nchannel]) # to keep the opd corresponding to the best fit
        bestAstrometry = (0, 0) # ra/dec in mas
        bestChi2 = np.inf # best value found for the chi2
        for i in range(nra):
            for j in range(ndec):
                ra = ra_values[i]
                dec = dec_values[j]
                # get the opd for each dit and baseline corresponding to the ra/dec value
                opd = self.getOpd(ra, dec)
                for dit in range(self.ndit):
                    for c in baselines_tofit:
                        # interpolate the opd map at the correct opd
                        chi2Map[dit, i, j] = chi2Map[dit, i, j]+np.interp(opd[dit, c], opdRanges[c, :], opdFit["map"][dit, c, :])
        # now look for best chi2
        chi2Map_summed = np.nansum(chi2Map, axis = 0) # sum over dits, ignoring nans
        ind = np.where(chi2Map_summed == np.nanmin(chi2Map_summed))
        ra = ra_values[ind[0][0]]
        dec = dec_values[ind[1][0]]        
        bestOpds = self.getOpd(ra, dec)
        bestAstrometry = (ra, dec)
        bestChi2 = chi2Map_summed[i, j]
        # now we can recalculate the best fit corresponding to the best astrometry
        # since the opd will be different for each dit, we need to construct it dit by dit
        opd_dit = np.zeros([self.nchannel, 1]) # to call fitVisRefOpd we need only one value on second axis
        fit = np.zeros([self.ndit, self.nchannel, self.nwav], "complex")
        params = np.zeros([self.ndit, self.nchannel, (poly_order+1)*2+1])
        for dit in range(self.ndit): 
            opd_dit[:, 0] = bestOpds[dit, :]
            opdfit = self.fitVisRefOpd(opd_dit, target_spectrum = target_spectrum, poly_spectrum = poly_spectrum, poly_order = poly_order, no_inv = no_inv, dits = [dit])
            fit[dit, :, :] = opdfit["fit"][dit, :, :]
            params[dit, :, :] = opdfit["params"][dit, :, 0, :]
        astrometryFit = {"map": chi2Map, "opdFit": opdFit, "best": bestAstrometry, "fit": fit, "params": params}
        if return_opd_fit:
            return astrometryFit, opdFit
        return astrometryFit
        
    
