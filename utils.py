# -*- coding: utf-8 -*-
"""
Some functions used here and there, which do not belong to any other file
@author: mnowak
"""

import numpy as np
from astropy.io import fits
from datetime import datetime


def diagonalMatrix(array):
    """
    create a matrix filled with zeros with given 1d array on diagonal.
    if array has more than 1 dimension, multiple matrices are returned
    with res[k1, k2, etc., kn, :, :] being the diagonalMatrix corrsponding 
    to array[k1, k2, etc., kn, :].
    """
    s = np.shape(array)
    snew = s + (s[-1], ) # shape of the returned array of matrices
    flat = array.flatten()
    res = np.zeros([len(flat), s[-1]], "complex")
    for k in range(len(flat)//s[-1]):
        res[k*s[-1]:(k+1)*s[-1], :] = np.diag(flat[k*s[-1]:(k+1)*s[-1]])
    return res.reshape(snew)

def loadFitsSpectrum(filename):
    """ Open a fits file written in GRAVITY convention, and return the wav grid, flux, covariance, contrast, and contrast covariance)
    """
    hdu = fits.open(filename)
    spectrum = hdu[1]
    wav = spectrum.data.field("wavelength")
    flux = spectrum.data.field("flux")
    fluxErr = spectrum.data.field("covariance")
    contrast = spectrum.data.field("contrast")        
    contrastErr = spectrum.data.field("covariance_contrast")    
    return wav, flux, fluxErr, contrast, contrastErr


def saveFitsSpectrum(filename, wav, flux, fluxCov, contrast, contrastCov, name = "Unknown", date_obs = "Unknown", mjd = "Unknown", instrument="GRAVITY", resolution = "Unknown", header = None):
    """Save a spectrum and contrast spectrum as a fits file
    Parameters:
      wav: 1d array containing the wavelength grid (in um)
      flux: 1d array containing the spectrum flux (W/m2/um)
      fluxCov: 2d array containing the covariance matrix on the spectrum flux (W/m2/um)^2.
      contrast: 1d array containing the contrast spectrum (unitless)
      contrastCov: 2d array containing covariance matrix on the contrast spectrum (unitless)    
    """
    if header is None:
        hdr = fits.Header()
        hdr['INSTRU'] = instrument
        hdr['FACILITY'] = 'ESO VLTI'
        hdr['DATE'] = str(datetime.utcnow())
        hdr['DATE-OBS'] = date_obs
        hdr['MJD-OBS'] = mjd
        hdr['OBJECT'] = name
        hdr['SPECRES'] = resolution        
        hdr['COMMENT'] = "FITS file contains multiple extensions"
    else:
        hdr = fits.Header()    
        hdr['COMMENT'] = "FITS file contains multiple extensions"
        if not(header is None):
            for key in header:
                hdr[key] = header[key]
    primary_hdu = fits.PrimaryHDU(header = hdr)
    col0= fits.Column(name='WAVELENGTH', format='1D', unit="um",  array=wav)
    col1= fits.Column(name='FLUX', format='1D', unit="W/m2/um",  array=flux)
    col2= fits.Column(name='COVARIANCE', format='%iD'%len(wav), unit="[W/m2/um]^2",  array=fluxCov)
    col3= fits.Column(name='CONTRAST', format='1D',unit=" - ",  array=contrast)
    col4= fits.Column(name='COVARIANCE_CONTRAST', format='%iD'%len(wav), unit=" - ^2",  array=contrastCov)
    secondary_hdu = fits.BinTableHDU.from_columns([col0, col1, col2, col3, col4])
    secondary_hdu.name='SPECTRUM'
    hdul = fits.HDUList([primary_hdu, secondary_hdu])
    hdul.writeto(filename, overwrite = True)
    return None


