# -*- coding: utf-8 -*-
"""
A class implementing VisOi with mode = "singlefield", with some additional fitting methods
@author: mnowak
"""
import numpy as np
import scipy
import astropy.io.fits as fits
from .genericOiFits import GenericOiFits 
from .visOiFits import VisOiFits
try:
    import patiencebar
    PBAR = True
except:
    PBAR = False


class VisOiFitsSinglefieldScivis(VisOiFits):
    """
    A class implementing VisOi with mode = "singlefield" and reduction "scivis", with some additional fitting methods    
    """
    def __init__(self, filename, extension = None, reduction = None):
        super(VisOiFitsSinglefieldScivis, self).__init__(filename = filename, extension = extension, reduction = "scivis", mode = 'singlefield')
        return None

    def detrendVisAmpDitByDit(self, order = 0):
        """
        Fit and remove a polynomial of given order to visAmp on a dit by dit basis
        """
        # to return srqsum
        sqrsum = np.zeros([self.ndit])
        # to return best coeffs
        coeffs = np.zeros([self.ndit, self.nchannel*(order+1)])
        self.visAmpFit = np.zeros([self.ndit, self.nchannel, self.nwav])        
        X = np.zeros([self.nchannel*self.nwav, (order+1)*self.nchannel])
        Y = np.zeros([self.nchannel*self.nwav, 1])
        # build the model X and the data Y
        for dit in range(self.ndit):
            for c in range(self.nchannel):
                visAmp = np.copy(self.visAmp[dit, c, :])
                freq = np.copy(self.freq[dit, c, :])
                ind = np.where(np.isnan(visAmp))[0]
                visAmp[ind] = 0
                freq[ind] = 0
                Y[c::self.nchannel, 0] = visAmp/self.visAmpErr[dit, c, :]
                for k in range(order+1):
                    X[c::self.nchannel, (order+1)*c+k] = ( (freq*1e-7)**k )/self.visAmpErr[dit, c, :]
                coeffs[dit, :] = np.dot(np.linalg.pinv(X), Y)[:, 0]
                Yfit = np.dot(X, coeffs[dit, :])
                # reshape Yfit and put it in visAmpFit
                for c in range(self.nchannel):
                    self.visAmpFit[dit, c, :] = Yfit[c::self.nchannel]*self.visAmpErr[dit, c, :]
                # calculate the sqrsum
                sqrsum[dit] = np.nansum((self.visAmpFit[dit, :, :] - self.visAmp[dit, :, :])**2/self.visAmpErr[dit, :, :]**2)
            # apply the detrending
            self.visAmp = self.visAmp - self.visAmpFit
        return (coeffs, self.visAmpFit, sqrsum)

    def fitOpdDitByDit(self, opdValue, channel = None, order = 0, spectrum = None):
        """
        Fit the fringes on one channel to find the best opd. Simultaneously fit a polynomial to detrend the data
        """
        if (channel is None):
            raise Exception("Channel keyword argument cannot be 'None'!")
        c = channel
        # to return srqsum
        sqrsums = np.zeros([self.ndit])
        bestFits = np.zeros([self.ndit, self.nwav])
        bestCoeffs = np.zeros([self.ndit, order+2])
        # to return best coeffs
        coeffs = np.zeros([self.ndit, (order+1)+1])
        X = np.zeros([self.nwav, (order+1)+1]) # +1 for the cosine term
        Y = np.zeros([self.nwav, 1])
        if spectrum is None:
            spectrum = np.ones(self.nwav)
        wav = self.wav
        cosine = spectrum[None, :]*np.cos(2*np.pi/(wav*1e6)*opdValue)
        # build the model X and the data Y
        for dit in range(self.ndit):
            visAmp = np.copy(self.visAmp[dit, c, :])
            ind = np.where(np.isnan(visAmp))[0]
            visAmp[ind] = 0
            Y[:, 0] = visAmp/self.visAmpErr[dit, c, :]
            X[:, -1] = cosine/self.visAmpErr[dit, c, :]
            X[ind, -1] = 0
            for k in range(order+1):
                X[:, k] = ( ((wav - np.mean(wav))*1e6)**k )/self.visAmpErr[dit, c, :]
                X[ind, k] = 0
            coeffs[dit, :] = np.dot(np.linalg.pinv(X), Y)[:, 0]
            if coeffs[dit, -1] < 0:
                X[:, -1] = 0
                coeffs[dit, :] = np.dot(np.linalg.pinv(X), Y)[:, 0]
                coeffs[dit, -1] = 0
            Yfit = np.dot(X, coeffs[dit, :])
            bestFits[dit, :] = Yfit*self.visAmpErr[dit, c, :]
            bestFits[dit, ind] = float('nan')
            bestCoeffs[dit, :] = coeffs[dit, :]
            # calculate the sqrsum
            residuals = (self.visAmp[dit, c, :] - bestFits[dit, :])**2/self.visAmpErr[dit, c, :]**2
            residuals[np.where(np.isnan(residuals))] = 0
            sqrsums[dit] = np.sum(residuals)
        return (bestCoeffs, bestFits, sqrsums, cosine)    

    def computeOpdChi2Maps(self, opdValues, order = 0, spectrum = None):
        nvalues = np.shape(opdValues)[1]
        chi2Maps = np.zeros([self.ndit, self.nchannel, nvalues]) # will contain the 1D chi-square for map for each channel and each dit
        chi2Mins = np.zeros([self.ndit, self.nchannel])+np.inf # will contain the best chi2
        opdBests = np.zeros([self.ndit, self.nchannel])+np.inf # will contain the opd corresponding to the best chi2
        bestFit = np.zeros([self.ndit, self.nchannel, self.nwav]) # will be filled with the best fits for each channel and each dit
        if PBAR:
            pbar = patiencebar.Patiencebar(valmax = nvalues, title = "Calculating the chi-square map...")
        for k in range(nvalues):
            # progress report to the user
            if PBAR:
                pbar.update()
            else:
                print(str(k+1)+"/"+str(nvalues))
            for c in range(self.nchannel):
                coeffs, Yfit, sqrsums, cosine = self.fitOpdDitByDit(opdValues[c, k], channel = c, spectrum = spectrum, order = order)
                chi2Maps[:, c, k] = sqrsums
                for dit in range(self.ndit):
                    if sqrsums[dit] < chi2Mins[dit, c]:
                        chi2Mins[dit, c] = sqrsums[dit]
                        opdBests[dit, c] = opdValues[c, k]
                        bestFit[dit, c, :] = Yfit[dit, :]
        self.opdfit = {"chi2Maps": chi2Maps, "chi2Mins": chi2Mins, "opdValues":opdValues, "bestFit": bestFit, "opdBests": opdBests}
        self.visAmpOpdFit = bestFit
        self.visAmpOpdFitMean = np.nanmean(bestFit, axis = 0)
        return None

    def computeChi2Map(self, xValues, yValues, nopds = None, recomputeOpdChi2Maps = True, spectrum = None, order = 0):
        """
        Use the opdChi2Map calculation to create a RA/DEC chi2 map, and extract the best fit in terms of RA/DEC from it
        RA and DEC values must be given in mas
        """
        nx = len(xValues)
        ny = len(yValues)
        if nopds is None:
            nopds = np.max([nx, ny])
        opds = np.zeros([self.ndit, self.nchannel, nx, ny])
        for i in range(nx):
            for j in range(ny):
                opds[:, :, i, j] = self.getOpd(xValues[i], yValues[j])
        if recomputeOpdChi2Maps:
            opdValues = np.zeros([self.nchannel, nopds])
            for c in range(self.nchannel):
                opdValues[c, :] = np.linspace(np.min(opds[:, c, :, :])-1, np.max(opds[:, c, :, :])+1, nopds)
            self.computeOpdChi2Maps(opdValues, order = order, spectrum = spectrum)
        opdChi2Maps = self.opdfit['chi2Maps']        
        allChi2Maps = np.zeros([self.ndit, nx, ny])
        chi2Map = np.zeros([nx, ny])                        
        for c in range(self.nchannel):
            for dit in range(self.ndit):
                f = scipy.interpolate.interp1d(opdValues[c, :], opdChi2Maps[dit, c, :], kind = 'quadratic', assume_sorted = True)
                allChi2Maps[dit, :, :] = allChi2Maps[dit, :, :] + f(opds[dit, c, :, :])
        chi2Map = np.sum(allChi2Maps, axis = 0)
        chi2Min = np.min(chi2Map)
        i, j = np.where(chi2Map == chi2Min)
        i, j = (i[0], j[0])
        xBest = xValues[i]
        yBest = yValues[j]
        # recalculate the best RA/DEC fit from single channel/dit fit in terms of OPD
        self.visAmpFit = np.zeros([self.ndit, self.nchannel, self.nwav])        
        for dit in range(self.ndit):
            for c in range(self.nchannel):
                opd = self.getOpd(xBest, yBest)[dit, c]                
                coeffs, Yfit, sqrsum, cosine = self.fitOpdDitByDit(opd, channel = c, spectrum = spectrum, order = order)
                self.visAmpFit[dit, c, :] = Yfit[dit, :]
        self.visAmpFitMean = np.nanmean(self.visAmpFit, axis = 0)
        # recalculate the best fits on a dit by dit basis
        allxBests = np.zeros(self.ndit)
        allyBests = np.zeros(self.ndit)
        allBestFits = np.zeros([self.ndit, self.nchannel, self.nwav])
        allCosines = np.zeros([self.ndit, self.nchannel, self.nwav])        
        allChi2Min = np.zeros(self.ndit)
        for dit in range(self.ndit):
            thisChi2Map = allChi2Maps[dit, :, :]
            thisChi2Min = np.min(thisChi2Map)
            i, j = np.where(thisChi2Map == thisChi2Min)
            i, j = (i[0], j[0])
            allxBests[dit] = xValues[i]
            allyBests[dit] = yValues[j]            
            for c in range(self.nchannel):
                opd = self.getOpd(allxBests[dit], allyBests[dit])[dit, c]                
                coeffs, thisYfit, sqrsum, cosine = self.fitOpdDitByDit(opd, channel = c, spectrum = spectrum, order = order)
                allBestFits[dit, c, :]= thisYfit[dit, :]
                allCosines[dit, c, :]= cosine[dit, :]
        # calculate error bars
        xChi2Map = np.min(chi2Map - chi2Min, axis = 1)
        yChi2Map = np.min(chi2Map - chi2Min, axis = 0)
        level = 1.0 # 68%
        xRange = [xValues[np.min(np.where(xChi2Map < level))], xValues[np.max(np.where(xChi2Map < level))]]
        yRange = [yValues[np.min(np.where(yChi2Map < level))], yValues[np.max(np.where(yChi2Map < level))]]
        xErr = 0.5*(xRange[1]-xRange[0])
        yErr = 0.5*(yRange[1]-yRange[0])        
        self.fit = {"allChi2Maps": allChi2Maps, "allxBests": allxBests, "allyBests": allyBests, "allBestFits": allBestFits, "chi2Map": chi2Map, "chi2Min": chi2Min, "xBest": xBest, "yBest": yBest, "bestFit": Yfit, "xErr": xErr, "yErr": yErr, "allCosines": allCosines}
        print("Best position found: RA = "+str(xBest)+" mas; DEC = "+str(yBest)+" mas.")
        return None
    
    def fitPosition(self, theta, sep, order = 0, spectrum = None):
        """
        Fit the fringes on one channel to find the best opd. Simultaneously fit a polynomial to detrend the data
        """
        # to return srqsum
        sqrsums = np.zeros([self.ndit])
        # to return bestFits
        bestFits = np.zeros([self.ndit, self.nchannel, self.nwav])
        # to return coeffs
        coeffs = np.zeros([self.ndit, self.nchannel*(order+1)+1])
        X = np.zeros([self.nwav*self.nchannel, (order+1)*self.nchannel+1]) # +1 for the cosine term
        Y = np.zeros([self.nwav*self.nchannel, 1])
        if spectrum is None:
            spectrum = np.ones(self.nwav)
        wav = self.wav
        theta = theta/360.0*2*np.pi
        this_u = (np.sin(theta)*self.uCoord + np.cos(theta)*self.vCoord)
        cosine = np.cos(2*np.pi*this_u*sep/3600.0/360.0/1000.0*2*np.pi)
        # build the model X and the data Y
        for dit in range(self.ndit):
            for c in range(self.nchannel):
                visAmp = np.copy(self.visAmp[dit, c, :])
                freq = np.copy(self.freq[dit, c, :])
                ind = np.where(np.isnan(visAmp))
                visAmp[ind] = 0
                Y[c::self.nchannel, 0] = visAmp/self.visAmpErr[dit, c, :]
                thiscosine = cosine[dit, c, :]/self.visAmpErr[dit, c, :]
                thiscosine[ind] = 0
                X[c::self.nchannel, -1] = thiscosine
                for k in range(order+1):
                    line = ( (freq*1e-7)**k )/self.visAmpErr[dit, c, :]
                    line[ind] = 0
                    X[c::self.nchannel, c*(order+1)+k] = line
            coeffs[dit, :] = np.dot(np.linalg.pinv(X), Y)[:, 0]
            Yfit = np.dot(X, coeffs[dit, :])
            for c in range(self.nchannel):
                bestFits[dit, c, :] = Yfit[c::self.nchannel]*self.visAmpErr[dit, c, :]
                ind = np.where(np.isnan(self.visAmp[dit, c, :]))
                bestFits[dit, c, ind] = float('nan')
            # calculate the sqrsum
            residuals = (self.visAmp[dit, :, :] - bestFits[dit, :, :])**2/(self.visAmpErr[dit, :, :]**2)
            ind = np.where(np.isnan(residuals))
            residuals[ind] = 0
            sqrsums[dit] = np.sum(residuals)
        return (coeffs, bestFits, sqrsums, cosine)    

    def computeChi2MapDirectly(self, raValues, decValues, spectrum = None, order = 0, telescope_based_contrast = False):
        """
        Directly comput ethe chi2Map in ra dec coorinates
        """
        if telescope_based_contrast:
            raise Exception("Telescope based contrast option not implemented")
        nRa = len(raValues)
        nDec = len(decValues)
        chi2Maps = np.zeros([self.ndit, nRa, nDec])
        bestFits = np.zeros([self.ndit, self.nchannel, self.nwav])
        raBests = np.zeros(self.ndit)                
        decBests = np.zeros(self.ndit)
        contrastBests = np.zeros(self.ndit)
        Y = np.zeros([self.nwav*self.nchannel, 1])
        X = np.zeros([self.nwav*self.nchannel, (self.nchannel*(order+1))+1])
        if PBAR:
            pbar = patiencebar.Patiencebar(valmax = self.ndit*nRa, title = "Calculating the chi-square map...")        
        for dit in range(self.ndit):
            chi2Min = np.inf
            for k in range(nRa):
                if PBAR:
                    pbar.update()
                for j in range(nDec):
                    ra = raValues[k]
                    dec = decValues[j]
                    this_freq = (ra*self.uCoord[dit, :, :] + dec*self.vCoord[dit, :, :])/3600.0/360.0/1000.0*2*np.pi
                    cosine = np.cos(2*np.pi*this_freq)
                    for c in range(self.nchannel):
                        Y[c*self.nwav:(c+1)*self.nwav, 0] = self.visAmp[dit, c, :]/self.visAmpErr[dit, c, :]
                        for o in range(order+1):
                            X[c*self.nwav:(c+1)*self.nwav, c*(order+1)+o] = ((self.wav-self.wav.mean())**o)/self.visAmpErr[dit, c, :]
                        X[c*self.nwav:(c+1)*self.nwav, -1] = cosine[c, :]/self.visAmpErr[dit, c, :]
                    A = np.dot(np.linalg.pinv(X), Y)
                    if (A[-1, 0] < 0):
                        A[-1, 0] = 0
                        X[:, -1] = 0
                        A = np.dot(np.linalg.pinv(X), Y)                        
                    Yfit = np.dot(X, A)
                    sqrsum = np.sum((Y - Yfit)**2)
                    chi2Maps[dit, k, j] = sqrsum
                    if sqrsum < chi2Min:
                        chi2Min = sqrsum
                        for c in range(self.nchannel):
                            bestFits[dit, c, :] = Yfit[c*self.nwav:(c+1)*self.nwav, 0]*self.visAmpErr[dit, c, :]
                        raBests[dit] = ra
                        decBests[dit] = dec
                        contrastBests[dit] = A[-1, 0]
        fit = {"chi2Maps": chi2Maps, "allBestFits": bestFits, "raBests": raBests, "decBests": decBests, "contrastBests": contrastBests}
        self.fit = fit
        return None

            
                        
                
